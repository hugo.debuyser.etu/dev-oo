package tp08;

import java.time.LocalDate;

public class Food implements IProduct, Comparable<Food>{
    private String label;
    private double price;
    private LocalDate bestBeforeDate;
    private static int compt;

    public Food(String label, double price, LocalDate bestBeforeDate){
        this(label, price);
        this.bestBeforeDate = bestBeforeDate;
    }

    public Food(String label, double price){
        if(label == null){
            this.label = "refUnknown" + compt;
            ++compt;    
        }
        else{
            this.label = label;
        }
        this.price = price;
        this.bestBeforeDate = LocalDate.now().plusDays(10);
    }

    public String getLabel(){
        return this.label;
    }

    public double getPrice(){
        return this.price;
    }

    public LocalDate getBestBeforeDate(){
        return this.bestBeforeDate;
    }

    public void setLabel(String label){
        this.label = label;
    }

    public void setPrice(double price){
        this.price = price;
    }

    public void setBestBeforeDate(LocalDate bestBeforeDate){
        this.bestBeforeDate = bestBeforeDate;
    }

    public boolean isPerishable() {
        return true;
    }

    public boolean isBestBefore(LocalDate aDate) {
        return bestBeforeDate.isBefore(aDate);
    }

    public String toString(){
        return "[" + this.label + "=" + this.price + " -> before " + this.bestBeforeDate;
    }

    @Override
    public int compareTo(Food other) {
        return this.bestBeforeDate.compareTo(other.bestBeforeDate);
    }
}

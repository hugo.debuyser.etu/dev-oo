package tp08;

import java.util.Objects;

public class Participant {
    private String name;
    private Participant partner;

    public Participant(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Participant getPartner() {
        return partner;
    }

    public void setPartner(Participant partner) {
        this.partner = partner;
    }

    public String getPartnerName(){
        return this.partner.name;
    }

    public boolean isMatched(){
        return partner != null;
    }
    public boolean isMatchedWith(Participant p){
        return partner != null && partner.equals(p);
    }

    public String toString(){
        return "[" + this.getName() + " -> " + this.getPartner() + "]";
    }

    public boolean matchWith(Participant p){
        if (this.partner == null && p.partner == null) {
            this.partner = p;
            p.partner = this;
            return true;
        }
        return false;
    }

    public void breakOff(){
        if (this.partner != null) {
            Participant temp = this.partner;
            this.partner = null;
            temp.partner = null;
        }
    }

    public int hashCode() {
        return Objects.hash(name);
    }

    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }
        if (obj == null || getClass() != obj.getClass()){
            return false;
        }
        Participant other = (Participant) obj;
        if(this.name != other.name){
            return false;
        }
        if(this.partner != other.partner){
            return false;
        }
        return true;
    }

}

package tp08;

import java.util.ArrayList;

public class Shelf {
    private ArrayList<IProduct> shelves;
    private boolean refrigerated;
    private int capacityMax;
    
    public Shelf(boolean refrigerated, int capacityMax){
        this.refrigerated = refrigerated;
        this.capacityMax = capacityMax;
        this.shelves = new ArrayList<>();
    }

    public ArrayList<IProduct> getShelves() {
        return shelves;
    }

    public int getCapacityMax() {
        return capacityMax;
    }

    public boolean isFull(){
        return this.shelves.size()>=this.capacityMax;
    }

    public boolean isEmpty(){
        return this.shelves.isEmpty();
    }

    public boolean isRefrigerated(){
        return this.refrigerated;
    }

    public boolean add(IProduct p) {
        if (isFull()) return false;
        if (p.isPerishable() && !refrigerated) return false;
        shelves.add(p);
        return true;
    }

    public String toString() {
        return "[" + this.isRefrigerated() + " : " + this.getCapacityMax() + " -> + " + this.shelves.toString() + "]"; 
    }
}

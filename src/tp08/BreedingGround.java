package tp08;

import java.util.HashSet;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.Random;

public class BreedingGround {
    private Set<Participant> applicants = new HashSet<>();

    public Set<Participant> getApplicants() {
        return applicants;
    }

    public boolean registration(Participant p) {
        return applicants.add(p);
    }

    public List<Participant> loners() {
        List<Participant> loners = new ArrayList<>();
        for (Participant p : applicants) {
            if (!p.isMatched()) {
                loners.add(p);
            }
        }
        return loners;
    }

    public List<Participant> lonersCleansing() {
        List<Participant> loners = loners();
        applicants.removeAll(loners);
        return loners;
    }

    public void forcedMatching() {
        List<Participant> loners = loners();
        Random random = new Random();

        while (loners.size() > 1) {
            Participant p1 = loners.remove(random.nextInt(loners.size()));
            Participant p2 = loners.remove(random.nextInt(loners.size()));
            p1.matchWith(p2);
        }
    }

    public List<Participant> cheaters(BreedingGround anotherBreedingGround) {
        List<Participant> cheaters = new ArrayList<>();
        for (Participant p : applicants) {
            if (anotherBreedingGround.getApplicants().contains(p)) {
                cheaters.add(p);
            }
        }
        return cheaters;
    }

    public void isolateCheater(Participant cheater) {
        if (cheater.isMatched()) {
            cheater.breakOff();
        }
    }

    public void cheatersCleansing(BreedingGround anotherBreedingGround) {
        List<Participant> cheaters = cheaters(anotherBreedingGround);
        for (Participant cheater : cheaters) {
            isolateCheater(cheater);
            applicants.remove(cheater);
            anotherBreedingGround.getApplicants().remove(cheater);
        }
    }

    public boolean possibleMerging(BreedingGround anotherBreedingGround) {
        return cheaters(anotherBreedingGround).isEmpty();
    }

    public void merging(BreedingGround anotherBreedingGround) {
        applicants.addAll(anotherBreedingGround.getApplicants());
    }

    public void securedMerging(BreedingGround anotherBreedingGround) {
        cheatersCleansing(anotherBreedingGround);
        if (possibleMerging(anotherBreedingGround)) {
            merging(anotherBreedingGround);
        }
    }

    public void clear() {
        for (Participant p : applicants) {
            p.breakOff();
        }
        applicants.clear();
    }



}

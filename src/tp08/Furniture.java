package tp08;

public class Furniture implements IProduct {
    private String label;
    private double price;
    private static int compt;

    public Furniture(String label, double price){
        if(label == null){
            this.label = "refUnknown" + compt++;
        }
        else{
            this.label = label;
        }
        this.price = price;
    }

    public String getLabel() {
        return label;
    }

    public double getPrice() {
        return price;
    }

    public boolean isPerishable() {
        return false;
    }

    public String toString(){
        return "[" + this.label + "=" + this.price;
    }

}

package tp08;

import java.time.LocalDate;
import java.util.ArrayList;

public class Shop {
    private ArrayList<Shelf> shelving;

    public Shop(){
        this.shelving = new ArrayList<>();
    }
    public Shop(ArrayList<Shelf> shelving){
        this.shelving = shelving;
    }

    public String toString(){
        return this.shelving.toString();
    }

    public ArrayList<Shelf> getShelving(){
        return this.shelving;
    }

    public void tidy(ArrayList<IProduct> aStock) {
        for (IProduct product : aStock) {
            for (Shelf shelf : shelving) {
                if (shelf.add(product)) {
                    break;
                }
            }
        }
    }

    public ArrayList<IProduct> closeBestBeforeDate(int nbDays) {
        ArrayList<IProduct> closeToExpiry = new ArrayList<>();
        LocalDate currentDate = LocalDate.now().plusDays(nbDays);
        for (Shelf shelf : shelving) {
            for (IProduct product : shelf.getShelves()) {
                if (product.isPerishable() && ((Food) product).isBestBefore(currentDate)) {
                    closeToExpiry.add(product);
                }
            }
        }
        return closeToExpiry;
    }
    
}
//Hugo DEBUYSER
package tp01;

public class Score {

    //attributes
    String name;
    int score;
    String timestamp;

    //constructor
    public Score(String name, int score, String timestamp){
        this.name = name;
        this.score = score;
        this.timestamp = timestamp;
    }

    //methods
    public String toString(){
        return "(" + this.timestamp + ")" + this.name + "=" + this.score;
    }

}

package tp01;

public class UseBook {
    public static void main(String[] args) {
        Book[] biblio = new Book[3];
        biblio[0] = new Book(null, null, 0);
        biblio[1] = new Book(null, null, 0);
        biblio[2] = new Book(null, null, 0);
        for(int i = 0; i<biblio.length; i++){
            System.out.println(biblio[i].author + " à écrit " + biblio[i].title + " en " + biblio[i].year);
        }
    }
}

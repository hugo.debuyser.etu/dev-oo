package tp01;

public class Book {
        // class attributes
        String author;
        String title;
        int year;
        // constructor
        Book(String author, String title, int year) {
            this.author = author;
            this.title = title;
            this.year = year;
        }
        // methods
        String getAuthor() {
            return this.author;
        }
        String getTitle() {
            return this.title;
        }
        String print() {
            return author + "\t" + title + "\t" + year;
        }

        public static void main(String[] args) {
            Book a = new Book("Auteur", "titre", 2000);
            System.out.println(a.author + " à écrit " + a.title + " en " + a.year);
        }

}

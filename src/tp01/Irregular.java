//hugo debuyser

package tp01;

public class Irregular {
    
    //attributes
    int [][]tab;

    //constructor
    public Irregular(int[] linesize){
        tab = new int[linesize.length][];
        for(int i = 0; i<linesize.length;i++){
            tab[i] = new int[linesize[i]];
        }
    }

    //methods
    public void randomFilling(){
        for(int i = 0; i<this.tab.length;i++){
            for(int j = 0; j<this.tab[i].length;j++){
                this.tab[i][j] = (int) (Math.random() * 100);
            }
        }
    }

    public boolean isCommun(int element){
        boolean trouvee = true;
        for(int i = 0; i < this.tab.length;i++){
            boolean trouveeActu = false;
            for(int j = 0; j<this.tab[i].length;j++){
                if(this.tab[i][j] == element){
                    trouveeActu = true;
                }
            }
            if(!trouveeActu){
                trouvee = false;
            }
        }
        return trouvee;
    }

    public boolean existCommon(){
        for(int i = 0;i<this.tab.length;i++){
            for(int j = 0;j<this.tab[i].length;j++){
                if(isCommun(this.tab[i][j])){
                    return true;
                }
            }
        }
        return false;
    }

    public String display(){
        StringBuilder renvoie = new StringBuilder();
        for(int i = 0; i<tab.length;i++){
            for(int j = 0; j<tab[i].length;j++){
                renvoie.append(tab[i][j]);
            }
            renvoie.append("\n");
        }
        return renvoie.toString();
    }

}

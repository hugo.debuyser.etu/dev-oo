package tp01;

public class UseHighScore {
    public static void main(String[] args) {
        HighScore highScore = new HighScore(3);
        System.out.println(highScore.toString());
        highScore.ajout(new Score("Alice", 300, "29/01"));
        System.out.println(highScore.toString());
        highScore.ajout(new Score("Bob", 800, "29/01"));
        System.out.println(highScore.toString());
        highScore.ajout(new Score("Alice", 42, "30/01"));
        System.out.println(highScore.toString());
        highScore.ajout(new Score("Alice", 650, "31/01"));
        System.out.println(highScore.toString());
    }
}
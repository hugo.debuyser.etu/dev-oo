package tp01;
import java.util.Random;

public class RandomSequence {
    public static void main(String[] args) {
        if(args.length<2 || args.length>3){
            System.out.println("Correct usage : <nbElt> <maxVal> [INTEGER|REAL]");
        }
        else{
            int nbElt = Integer.parseInt(args[0]);
            int maxVal = Integer.parseInt(args[1]);
            boolean isReal = args.length == 3 && args[2].equals("REAL");
            Random alea = new Random();
            for(int i = 0; i<nbElt;i++){
                if(isReal){
                    System.out.println(alea.nextDouble() * maxVal+1);
                }
                else{
                    System.out.println(alea.nextInt(maxVal+1));
                }
            }
        }
    }
}

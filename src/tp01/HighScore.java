//Hugo DEBUYSER
package tp01;

public class HighScore {
    //attributes
    private Score[] top;

    //constructor
    public HighScore(int size){
        this.top = new Score[size];
    }

    //methods
    private int binarySearch(Score[] scores, int target, int debut, int fin) {
        while (debut <= fin) {
            int milieu = (debut + fin) / 2;
            if (scores[milieu] != null) {
                if (scores[milieu].score > target) {
                    debut = milieu + 1;
                } else if (scores[milieu].score < target) {
                    fin = milieu - 1;
                } else {
                    return milieu;
                }
            } else {
                fin = milieu - 1;
            }
        }
        return -(debut + 1);
    } 

    public boolean ajout(Score newScore) {
        if (top[0] == null) {
            top[0] = newScore;
            return true;
        }
        int index = binarySearch(top, newScore.score, 0, top.length - 1);
        if (index < 0) {
            index = -(index + 1);
        }
        for (int i = top.length - 1; i > index; i--) {
            top[i] = top[i - 1];
        }
        top[index] = newScore;
        return true;
    }

    public String toString() {
        String rendue = "TOP SCORE:\n";
        for (int i = 0; i < top.length && top[i] != null; i++) {
            rendue += top[i].toString() + "\n";
        }
        return rendue;
    }

}

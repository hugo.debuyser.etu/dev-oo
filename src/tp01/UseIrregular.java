package tp01;

public class UseIrregular {
    public static void main(String[] args) {
        Irregular irregular = new Irregular(new int[]{3, 2, 1});
        irregular.randomFilling();
        System.out.println(irregular.display());

        int commonElement = 50;
        System.out.println(commonElement + ":" + irregular.isCommun(commonElement));
        System.out.println(irregular.existCommon());
    }
}

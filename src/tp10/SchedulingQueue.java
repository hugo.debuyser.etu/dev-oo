package tp10;

import java.util.LinkedList;

public class SchedulingQueue<T> implements IScheduler<T>{
    protected LinkedList<T> theQueue;
    public static int queueCounter=0;
    public final int ID;

    public SchedulingQueue(){
        this.theQueue = new LinkedList<>();
        ID=queueCounter;
        SchedulingQueue.queueCounter++;
    }

    public int getID(){
        return this.ID;
    }

    @Override
    public void addElement(T t){
        theQueue.add(t);
    }

    @Override
    public T highestPriority(){
        if(!this.isEmpty()){
            T renvoie = this.theQueue.getFirst();
            this.theQueue.removeFirst();
            return renvoie;
        }
        return null;
    }

    @Override
    public boolean isEmpty(){
        return(this.theQueue.isEmpty());
    }

    @Override
    public int size(){
        return(this.theQueue.size());
    }

    public String toString(){
        return "Queue" + getID() + " -> " + this.theQueue.toString();
    }
}

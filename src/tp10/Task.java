package tp10;

public class Task implements IPriority{
    private int priority;
    private String label;

    public Task(String label, int priority){
        this.priority = priority;
        this.label = label;
    }

    public int getPriority(){
        return this.priority;
    }

    public String getLabel(){
        return this.label;
    }

    public void setLabel(String label){
        this.label = label;
    }

    public String toString(){
        return getClass().getName() + ":" + this.label + "(" + this.priority + ")";
    }

    

}

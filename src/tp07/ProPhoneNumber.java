package tp07;

import td01.PhoneNumber;

public class ProPhoneNumber {
    private UniversityDepartment dept;
    private PhoneNumber numbers;

    public ProPhoneNumber(String numbers, UniversityDepartment dept){
        this.dept = dept;
        this.numbers = new PhoneNumber(33, 03, 20,
        this.dept.getDiallingCode(),
            Integer.parseInt(numbers.substring(0, 2)),
            Integer.parseInt(numbers.substring(2, 4)));
    }

    public UniversityDepartment getDepartment() {
        return this.dept;
    }

    public PhoneNumber getNumbers() {
        return this.numbers;
    }

    public String internToString(){
        return this.dept.getDiallingCode() +
        this.numbers.toString().substring(9, 11) +
        this.numbers.toString().substring(12, 14) + 
        "(" + this.dept.getLabelShort() + ")";
    }

    public String externToString(){
        return this.numbers.internationalFormat() + " (" + this.dept.getLabelLong() + ")";
    }

    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ProPhoneNumber other = (ProPhoneNumber) obj;
        if (dept != other.dept)
            return false;
        if (numbers == null) {
            if (other.numbers != null)
                return false;
        } else if (!numbers.equals(other.numbers))
            return false;
        return true;
    }

    public boolean equals(String fourDigits){
        String lastDigits = this.internToString().substring(1, 5);
        if(lastDigits.equals(fourDigits)) return true;
        else return false;
    }

}

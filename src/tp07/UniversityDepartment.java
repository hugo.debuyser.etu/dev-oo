package tp07; // à adapter éventuellement selon la structure de votre projet

public enum UniversityDepartment {
    F3S(1,"Faculté des Sciences de Santé et du Sport"),
    FST(2,"Faculté des Sciences et Technologies"),
    IUT(3,"Institut Universitaire de Technologies"),
    FSJPS(4,"Faculté des Sciences Juridiques, Politiques et Sociales"),
    FSEST(5,"Faculté des Sciences Économiques, Sociales et des Territoires"),
    FH(6,"Faculté des Humanités"),
    FLCS(7,"Faculté des Langues, Cultures et Sociétés"),
    FPSEF(8,"Faculté de Psychologie, Sciences de l’Education et de la Formation");

    private final int DIALLINGCODE;
    private final String LABELSHORT;
    private final String LABELLONG;

    private UniversityDepartment(int diallingCode, String labelShort){
        this.DIALLINGCODE = diallingCode;
        this.LABELLONG = labelShort;
        this.LABELSHORT = this.name();
    }

    public int getDiallingCode() {
        return this.DIALLINGCODE;
    }

    public String getLabelShort() {
        return this.LABELSHORT;
    }

    public String getLabelLong() {
        return this.LABELLONG;
    }
}

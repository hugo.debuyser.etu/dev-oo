package tp07;

public abstract class Poultry {
    protected int identity;
    protected double weight;

    protected Poultry(int identity,double weight){
        this.identity = identity;
        this.weight = weight;
    }

    public int getIdentity() {
        return identity;
    }

    public void setIdentity(int identity) {
        this.identity = identity;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public abstract double getPriceKg();

    public abstract double getSlaughterThreshold();

    public double getPrice(){
        return this.weight * getPriceKg();
    }

    
    public boolean isFatEnough(){
        if(this.weight>=getSlaughterThreshold()){
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " [" + this.identity + "," + this.weight + "]";
    }

}

package tp07;

public class Duck extends Poultry implements IForceFeeding{

    private static double priceKg = 1.5;
    private static double slaughterThreshold = 5.;

    public Duck(int identity, double weigth){
        super(identity, weigth);
    }

    @Override
    public double getPriceKg() {
        return priceKg;
    }

    @Override
    public double getSlaughterThreshold() {
        return slaughterThreshold;
    }

    public static void setPriceKg(double priceKg) {
        Duck.priceKg = priceKg;
    }

    public static void setSlaughterThreshold(double slaughterThreshold) {
        Duck.slaughterThreshold = slaughterThreshold;
    }

    @Override
    public double getLuxuryPrice() {
        return 2 * getPrice();
    }

}

package tp07;

public class Goose extends Poultry implements IForceFeeding{

    private static double priceKg = 4.;
    private static double slaughterThreshold = 10.;

    public Goose(int identity, double weigth){
        super(identity, weigth);
    }

    @Override
    public double getPriceKg() {
        return priceKg;
    }

    @Override
    public double getSlaughterThreshold() {
        return slaughterThreshold;
    }

    public static void setPriceKg(double priceKg) {
        Goose.priceKg = priceKg;
    }

    public static void setSlaughterThreshold(double slaughterThreshold) {
        Goose.slaughterThreshold = slaughterThreshold;
    }

    @Override
    public double getLuxuryPrice() {
        return 2 * getPrice();
    }

}

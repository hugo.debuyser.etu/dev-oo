package tp07;

import java.util.ArrayList;
import java.util.List;

public class BreedingFarm {
    private int maxSize;
    private List<Poultry> beasts;

    public BreedingFarm(){
        this(200);
    }

    public BreedingFarm(int maxSize){
        this.maxSize = maxSize;
        this.beasts = new ArrayList<>();
    }

    public int getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public List<Poultry> getBeasts() {
        return beasts;
    }

    public void setBeasts(List<Poultry> beasts) {
        this.beasts = beasts;
    }

    public void add(Poultry beast){
        if(this.beasts.size()<this.maxSize){
            this.beasts.add(beast);
        }
    }

    public int getFarmSize(){
        return this.beasts.size();
    }

    public int getNbDuck(){
        int renvoie = 0;
        for(Poultry beast : beasts){
            if(beast instanceof Duck){
                renvoie += 1;
            }
        }
        return renvoie;
    }

    public Poultry search(int identity){
        for(Poultry beast : beasts){
            if(beast.identity == identity){
                return beast;
            }
        }
        return null;
    }

    public Duck getDuck(int i){
        if(search(i) instanceof Duck){
            return (Duck)search(i);
        }
        return null;
    }

    public void updateWeight(int identity, double weight){
        search(identity).setWeight(weight);
    }

    public double potentialProfit(){
        double renvoie = 0;
        for(Poultry beast : beasts){
            if(beast.isFatEnough()){
                renvoie += beast.getPrice();
            }
        }
        return renvoie;
    }

    public List<Poultry> slaughtering(){
        List<Poultry> slaughteringBeasts = new ArrayList<>();
        for(Poultry beast : beasts){
            if(beast.isFatEnough()){
                slaughteringBeasts.add(beast);
            }
        }
        this.beasts.removeAll(slaughteringBeasts);
        return slaughteringBeasts;
    }

    public String toString(){
        String renvoie = "";
        for(Poultry beast : beasts){
            renvoie += beast + System.getProperty("line.separator");
        }
        return renvoie;
    }

}

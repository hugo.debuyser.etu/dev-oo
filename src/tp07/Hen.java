package tp07;

public class Hen extends Poultry implements IForceFeeding{

    private static double priceKg = 1.;
    private static double slaughterThreshold = 3.5;

    public Hen(int identity, double weigth){
        super(identity, weigth);
    }

    @Override
    public double getPriceKg() {
        return priceKg;
    }

    @Override
    public double getSlaughterThreshold() {
        return slaughterThreshold;
    }

    public static void setPriceKg(double priceKg) {
        Hen.priceKg = priceKg;
    }

    public static void setSlaughterThreshold(double slaughterThreshold) {
        Hen.slaughterThreshold = slaughterThreshold;
    }

    @Override
    public double getLuxuryPrice() {
        return 2 * getPrice();
    }

}

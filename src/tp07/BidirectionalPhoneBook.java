package tp07;

import java.util.Map;
import java.util.HashMap;

public class BidirectionalPhoneBook {
    private Map<String, ProPhoneNumber> name2num;
    private Map<String, String> num2name;

    public BidirectionalPhoneBook(){
        this.name2num = new HashMap<String, ProPhoneNumber>();
        this.num2name = new HashMap<String, String>();
    }

    public int getNbEntries(){
        return this.name2num.size();
    }

    public boolean alreadyRegistered(String s){
        return this.name2num.containsKey(s) || this.num2name.containsKey(s);
    }

    public boolean add(String name, UniversityDepartment dept, String fourDigits){
        if(alreadyRegistered(name) || alreadyRegistered("" + dept.getDiallingCode() + fourDigits)){
            return false;
        }
        this.name2num.put(name, new ProPhoneNumber(fourDigits, dept));
        this.num2name.put("" + dept.getDiallingCode() + fourDigits, name);
        return true;
    }

    public ProPhoneNumber getProPhoneNumberFromName(String name){
        return this.name2num.get(name);
    }

    public String getNameFromFiveDigits(String fiveDigits){
        return this.num2name.get(fiveDigits);
    }

    public String listing(UniversityDepartment dept){
        String renvoie = dept.getLabelLong() + System.getProperty("line.separator");
        for(String name : this.name2num.keySet()) {
            if (this.name2num.get(name).getDepartment().equals(dept)) {
                renvoie += name + ":" + this.name2num.get(name).internToString() + System.getProperty("line.separator");
            }
        }
        return renvoie;
    }

    public String listing(){
        String renvoie = "";
        for(UniversityDepartment i : UniversityDepartment.values()) {
            renvoie = renvoie + listing(i);
        }
        return renvoie;

    }

}

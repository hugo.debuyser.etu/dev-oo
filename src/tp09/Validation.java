package tp09;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Validation {
    public static boolean checkFile(String filename) throws FileNotFoundException, InputMismatchException, InvalidStructureException {
        File file = new File(filename);
        if (!file.exists()) {
            throw new FileNotFoundException("Le fichier n'existe pas");
        }
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                Scanner lineScanner = new Scanner(line);
                lineScanner.useDelimiter(",");

                if (!lineScanner.hasNextInt()) {
                    lineScanner.close();
                    throw new InvalidStructureException("Le premier champ doit être un entier représentant le type de données.");
                }
                int type = lineScanner.nextInt();

                if (!lineScanner.hasNextInt()) {
                    lineScanner.close();
                    throw new InvalidStructureException("Le second champ doit être un entier représentant le nombre de données.");
                }
                int count = lineScanner.nextInt();

                for (int i = 0; i < count; i++) {
                    if (!lineScanner.hasNext()) {
                        lineScanner.close();
                        throw new InvalidStructureException("Nombre de données incorrect pour la ligne: " + line);
                    }
                    if (type == 1) {
                        if (!lineScanner.hasNextBoolean()) {
                            lineScanner.close();
                            throw new InputMismatchException("Donnée attendue de type booléen.");
                        }
                        lineScanner.nextBoolean();
                    } else if (type == 2) {
                        if (!lineScanner.hasNextInt()) {
                            lineScanner.close();
                            throw new InputMismatchException("Donnée attendue de type entier.");
                        }
                        lineScanner.nextInt();
                    } else {
                        lineScanner.next();
                    }
                }

                if (lineScanner.hasNext()) {
                    lineScanner.close();
                    throw new InvalidStructureException("Données supplémentaires trouvées après le nombre attendu.");
                }

                lineScanner.close();
            }
        }
        return true;
    }

}

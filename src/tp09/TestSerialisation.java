package tp09;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.time.LocalDate;

public class TestSerialisation {

    public static void savingAnonymous(Anonymous johnDoe, String location) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(location))) {
            oos.writeObject(johnDoe);
            System.out.println("Anonymous object saved successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void loadingAnonymous(String location) {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(location))) {
            Anonymous anonymous = (Anonymous) ois.readObject();
            System.out.println("Objet chargé avec succès depuis " + location + ": " + anonymous);
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Erreur lors du chargement : " + e.getMessage());
        }
    }

    public static void main(String[] args) {
        Anonymous johnDoe = new Anonymous("Test", LocalDate.of(2020, 6, 1));
        savingAnonymous(johnDoe, "anonymous.ser");
        loadingAnonymous("anonymous.ser");
    }

}

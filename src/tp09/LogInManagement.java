package tp09;

import java.util.Scanner;

public class LogInManagement {
    public static final String LOGIN = "toto";
    public static final String PWD = "toto";

    public void getUserPwd() throws WrongLoginException, WrongPwdException, WrongInputLengthException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Entrez un login : ");
        String login = scanner.nextLine();
        System.out.print("Entrez un mdp : ");
        String pwd = scanner.nextLine();

        if (login.length() > 10 || pwd.length() > 10) {
            scanner.close();
            throw new WrongInputLengthException("Le login ou le mot de passe ne doivent pas dépasser 10 caractères");
        }

        if (!login.equals(LOGIN)) {
            scanner.close();
            throw new WrongLoginException("Login incorrect");
        }
        if (!pwd.equals(PWD)) {
            scanner.close();
            throw new WrongPwdException("Mot de passe incorrect");
        }
        scanner.close();
    }

    public static void main(String[] args) {
        LogInManagement logInManagement = new LogInManagement();
        boolean correctLogin = false;
        while (!correctLogin) {
            try {
                logInManagement.getUserPwd();
                System.out.println("Connexion réussie");
                correctLogin = true;
            } catch (WrongLoginException | WrongPwdException | WrongInputLengthException e) {
                System.out.println(e.getMessage());
            } catch (Exception e) {
                System.out.println("Une erreur s'est produite : " + e.getMessage());
            }
        }
    }
}

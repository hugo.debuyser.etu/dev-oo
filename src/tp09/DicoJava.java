package tp09;

import java.io.*;
import java.util.*;

public class DicoJava {
    public static void main(String[] args) {
        List<String> motsCles = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(new File("DicoJava.txt")))) {
            String line;
            while ((line = br.readLine()) != null) {
                Scanner sc = new Scanner(line).useDelimiter("\t");
                String mot = sc.next();
                String type = sc.next();
                if (type.equals("Mot-clé")) {
                    motsCles.add(mot);
                }
                sc.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        Collections.sort(motsCles);
        try (BufferedWriter bw = new BufferedWriter(new FileWriter("MotsJava.txt"))) {
            for (String mot : motsCles) {
                bw.write(mot);
                bw.newLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Extraction et sauvegarde des mots-clés réussies.");
    }
}

package tp09;

import java.io.File;
import java.io.IOException;

import util.HierarchyCreation;

public class ListExecutable {

    public static void printExecutable(File file) {
        if (file.exists()) {
            if (file.isDirectory()) {
                File[] files = file.listFiles();
                if (files != null) {
                    for (File f : files) {
                        printExecutable(f);
                    }
                }
            } else {
                if (file.canExecute() && !file.isHidden()) {
                    System.out.println(file.getAbsolutePath());
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        String testDirectoryPath = "testHierarchy";
        HierarchyCreation.hierarchyCreation(testDirectoryPath);
        File rootDirectory = new File(testDirectoryPath);
        printExecutable(rootDirectory);
    }
}
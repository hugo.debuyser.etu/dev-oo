package tp09;

public class InvalidStructureException extends Exception{
    public InvalidStructureException(String message) {
        super(message);
    }
}

package tp09;

import java.io.Serializable;
import java.time.LocalDate;

public class Anonymous implements Serializable {
    private static final long serialVersionUID = 1;

    private String pseudo;
    private LocalDate registration;

    public Anonymous(String pseudo, LocalDate registration) {
        this.pseudo = pseudo;
        this.registration = registration;
    }

    public String getPseudo() {
        return pseudo;
    }

    public LocalDate getRegistrationDate() {
        return registration;
    }

    public long getSeniority() {
        return registration.until(LocalDate.now()).toTotalMonths();
    }

    @Override
    public String toString() {
        return "Anonymous [" + pseudo + "," + getSeniority() + "]";
    }
}


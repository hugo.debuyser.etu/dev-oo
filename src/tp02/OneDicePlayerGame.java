//Hugo DEBUYSER

package tp02;

public class OneDicePlayerGame {
    public static void main(String[] args) {
        Dice dice = new Dice(6);
        DicePlayer player = new DicePlayer("Alice");

        while (player.getTotalvalue() < 20) {
            player.play(dice);
        }

        System.out.println(player);
    }
}

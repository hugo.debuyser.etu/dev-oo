//Hugo DEBUYSER
package tp02;

public class DicePlayer {

    //attributes
    private String name;
    private int totalValue;
    private int nbDiceRolls;

    //constructor
    public DicePlayer(String name){
        this.name = name;
        this.nbDiceRolls = 0;
        this.totalValue = 0;
    }

    public void play(Dice dice6){
        dice6.roll();
        totalValue = totalValue + Integer.parseInt(dice6.toString());
        this.nbDiceRolls++;
    }

    public String toString(){
        return this.name  + ": " + this.totalValue + " points en " + this.nbDiceRolls + " coups.";
    }

    public int getTotalvalue(){
        return this.totalValue;
    }

    public int getNbDiceRolls(){
        return this.nbDiceRolls;
    }

    public int getTotalValue(){
        return this.totalValue;
    }

    public boolean isWinning(DicePlayer other) {
        if (this.nbDiceRolls < other.nbDiceRolls) {
            return true;
        } else if (this.nbDiceRolls == other.nbDiceRolls) {
            return this.totalValue > other.totalValue;
        } else {
            return false;
        }
    }

}

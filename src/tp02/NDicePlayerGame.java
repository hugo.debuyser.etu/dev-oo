package tp02;

public class NDicePlayerGame {

    //attributes
    private DicePlayer[] players;

    //constructor
    public NDicePlayerGame(int numPlayers) {
        players = new DicePlayer[numPlayers];
        for (int i = 0; i < players.length; ++i) {
            players[i] = new DicePlayer(Integer.toString(i + 1));
        }
    }

    //methods
    public DicePlayer[] winner() {
        int minDiceRolls = Integer.MAX_VALUE;
        int count = 0;

        for (DicePlayer player : players) {
            int totalValue = player.getTotalValue();
            int nbDiceRolls = player.getNbDiceRolls();

            if (totalValue >= 20 && nbDiceRolls < minDiceRolls) {
                minDiceRolls = nbDiceRolls;
                count = 1;
            } else if (totalValue >= 20 && nbDiceRolls == minDiceRolls) {
                count++;
            }
        }

        DicePlayer[] winners = new DicePlayer[count];
        int index = 0;

        for (DicePlayer player : players) {
            int totalValue = player.getTotalValue();
            int nbDiceRolls = player.getNbDiceRolls();

            if (totalValue >= 20 && nbDiceRolls == minDiceRolls) {
                winners[index++] = player;
            }
        }

        return winners;
    }

    public static void main(String[] args) {
        int numPlayers = Integer.parseInt(args[0]);
        NDicePlayerGame game = new NDicePlayerGame(numPlayers);
        Dice dice6 = new Dice(6);

        boolean gameFinished = false;
        while (!gameFinished) {
            for (DicePlayer player : game.players) {
                player.play(dice6);
                if (player.getTotalValue() >= 20) {
                    gameFinished = true;
                }
            }
        }

        DicePlayer[] winners = game.winner();

        System.out.println("Les vainqueurs:");
        for (DicePlayer winner : winners) {
            System.out.println(winner);
        }
    }
}

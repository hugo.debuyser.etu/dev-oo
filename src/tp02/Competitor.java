//Hugo Debuyser

package tp02;

public class Competitor {

    //attribute
    private String numberSign;
    private int time;
    private int score;

    //constructor
    public Competitor(int numberSign, int score, int min, int sec){
        this.numberSign = String.valueOf(numberSign);
        this.time = min*60 + sec;
        this.score = score;
    }

    //methods
    public String display(){
        if(this.numberSign == null){
            return "[<invalide>" + ", " + this.score + " points, " + this.time + " s]";
        }
        return "[No" + this.numberSign + ", " + this.score + " points, " + this.time + " s]";
    }

    public boolean equals(Competitor other){
        if(other != null){
            return(other.numberSign.equals(this.numberSign) && other.score == this.score);
        }
        return false;
    }

    public boolean isFaster(Competitor other){
        if(other != null){
            return this.time < other.time;
        }
        return false;
    }

    public static void main(String[] args) {
        Competitor[] competitors = new Competitor[5];
        competitors[0] = new Competitor(1, 45, 15, 20);
        competitors[1] = new Competitor(2, 32, 12, 45);
        competitors[2] = new Competitor(5, 12, 13, 59);
        competitors[3] = new Competitor(12, 12, 15, 70);
        competitors[4] = new Competitor(32, 75, 15, 20);
        for(int i = 0; i<competitors.length; i++){
            if(competitors[i].numberSign != null){
                System.out.println(competitors[i].display());
            }
        }
    }
}

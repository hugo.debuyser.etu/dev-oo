//Hugo Debuyser
package tp02;

import java.util.Random;

public class Dice {
    //attributes
    private int numberSides;
    private Random rand;
    private int value;
    
    //constructor
    public Dice(int numberSides){
        if (numberSides > 0) {
            this.numberSides = numberSides;
        }
        else {
            this.numberSides = 1;
        }
        this.rand = new Random();
        this.roll();
    }
    
    //methods
    public void roll() {
        this.value = rand.nextInt(numberSides) + 1;
    }

    public String toString() {
        return String.valueOf(value);
    }
}

//Hugo DEBUYSER

package tp02;

public class TwoDicePlayerGame {

    //attributes
    private DicePlayer player1;
    private DicePlayer player2;

    //constructor
    public TwoDicePlayerGame(DicePlayer player1, DicePlayer player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    //methods
    public DicePlayer winner(){
        if (player1.isWinning(player2)) {
            return player1;
        } else {
            return player2;
        }
    }

    public static void main(String[] args) {
        DicePlayer alice = new DicePlayer("Alice");
        DicePlayer bob = new DicePlayer("Bob");

        TwoDicePlayerGame game = new TwoDicePlayerGame(alice, bob);
        Dice dice6 = new Dice(6);

        while (alice.getTotalvalue() < 20 && bob.getTotalvalue() < 20) {
            alice.play(dice6);
            bob.play(dice6);
        }

        DicePlayer winner = game.winner();
        System.out.println("Le gagnant est " + winner);
    }
}

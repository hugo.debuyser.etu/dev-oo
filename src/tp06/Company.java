package tp06;

import java.time.LocalDate;
import java.util.ArrayList;

/**
 * //Hugo D
 * Classe représentant une entreprise.
 */
public class Company {
    /**
     * Liste du personnel de l'entreprise.
     */
    private ArrayList<Employee> staff;

    /**
     * Constructeur de la classe Company.
     */
    public Company(){
        this.staff = new ArrayList<Employee>();
    }

    /**
     * Méthode pour ajouter un employé à l'entreprise.
     * @param e L'employé à ajouter.
     */
    public void addEmployee(Employee e){
        this.staff.add(e);
    }

    /**
     * Méthode pour supprimer un employé en utilisant son indice dans la liste.
     * @param index L'indice de l'employé à supprimer.
     */
    public void supprEmployee(int index){
        this.staff.remove(index);
    }

    /**
     * Méthode pour supprimer un employé en utilisant l'objet employé.
     * @param e L'employé à supprimer.
     */
    public void supprEmployee(Employee e){
        this.staff.remove(e);
    }

    /**
     * Méthode de représentation textuelle de l'entreprise.
     * @return Une représentation textuelle de l'entreprise.
     */
    public String toString(){
        return this.staff.toString();
    }

    /**
     * Méthode pour obtenir le nombre total d'employés dans l'entreprise.
     * @return Le nombre total d'employés.
     */
    public int getNbEmployee(){
        return getNbSalesPerson() + getNbWorker();
    }

    /**
     * Méthode pour obtenir le nombre de vendeurs dans l'entreprise.
     * @return Le nombre de vendeurs.
     */
    public int getNbSalesPerson(){
        int compteur = 0;
        for(int i = 0;i<this.staff.size();i++){
            if(this.staff.get(i).getTitle() == "Vendeur" || this.staff.get(i).getTitle() == "Voyageur"){
                compteur++;
            }
        }
        return compteur;
    }

    /**
     * Méthode pour obtenir le nombre d'employés travaillant à la production dans l'entreprise.
     * @return Le nombre d'employés travaillant à la production.
     */
    public int getNbWorker(){
        int compteur = 0;
        for(int i = 0;i<this.staff.size();i++){
            if(this.staff.get(i).getTitle() == "Employée"){
                compteur++;
            }
        }
        return compteur;
    }

    /**
     * Méthode pour licencier les employés embauchés après une certaine date.
     * @param fatefulDate La date à partir de laquelle les employés doivent être licenciés.
     */
    public void firing(LocalDate fatefulDate){
        int i = 0;
        while (i < getNbEmployee()){
            if(this.staff.get(i).getHiringDate().isAfter(fatefulDate)){
                supprEmployee(i);
            }
            else{
                ++i;
            }
        }
    }

    /**
     * Méthode pour licencier les employés dont l'objectif n'est pas atteint.
     */
    public void firing(){
        for(int i = 0;i<this.staff.size();i++){
            if(this.staff.get(i).objectiveFulfilled() == false){
                supprEmployee(i);
            }
        }
    }
}

package tp06;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

/**
 * @author Hugo D
 * Classe représentant une voiture.
 */
public class Car {
    /**
     * Taux de décote temporelle.
     */
    protected double temporalDropRate = 0.001;
    /**
     * Taux de décote liée au kilométrage.
     */
    protected double mileageDropRate = 0.002;
    /**
     * Marque de la voiture.
     */
    private String brand;
    /**
     * Date de mise en circulation de la voiture.
     */
    private LocalDate onRoad;
    /**
     * Prix d'achat de la voiture.
     */
    protected double purchasePrice;
    /**
     * Date de mise en vente de la voiture.
     */
    private LocalDate onSale;
    /**
     * Prix de vente de la voiture.
     */
    private double salePrice;
    /**
     * Kilométrage de la voiture.
     */
    private int mileage;

    /**
     * Constructeur avec toutes les informations sur la voiture.
     * @param brand Marque de la voiture.
     * @param onRoad Date de mise en circulation de la voiture.
     * @param pPrice Prix d'achat de la voiture.
     * @param onSale Date de mise en vente de la voiture.
     * @param sPrice Prix de vente de la voiture.
     * @param km Kilométrage de la voiture.
     */
    Car(String brand, LocalDate onRoad, double pPrice, LocalDate onSale, double sPrice, int km){
        this(brand, onRoad, pPrice, km);
        this.onSale = onSale;
        this.salePrice = sPrice;
    }

    /**
     * Constructeur sans spécifier la date de mise en vente et le prix de vente.
     * @param brand Marque de la voiture.
     * @param onRoad Date de mise en circulation de la voiture.
     * @param pPrice Prix d'achat de la voiture.
     * @param km Kilométrage de la voiture.
     */
    Car(String brand, LocalDate onRoad, double pPrice, int km){
        this.brand = brand;
        this.onRoad = onRoad;
        this.purchasePrice = pPrice;
        this.mileage = km;
    }

    /**
     * Accesseur pour la marque de la voiture.
     * @return La marque de la voiture.
     */
    public String getBrand(){
        return this.brand;
    }

    /**
     * Accesseur pour la date de mise en circulation de la voiture.
     * @return La date de mise en circulation de la voiture.
     */
    public LocalDate getOnRoad(){
        return this.onRoad;
    }

    /**
     * Accesseur pour le prix d'achat de la voiture.
     * @return Le prix d'achat de la voiture.
     */
    public double getPurchasePrice(){
        return this.purchasePrice;
    }

    /**
     * Accesseur pour la date de mise en vente de la voiture.
     * @return La date de mise en vente de la voiture.
     */
    public LocalDate getOnSale(){
        return this.onSale;
    }

    /**
     * Accesseur pour le kilométrage de la voiture.
     * @return Le kilométrage de la voiture.
     */
    public int getMileage(){
        return this.mileage;
    }

    /**
     * Accesseur pour le taux de décote temporelle.
     * @return Le taux de décote temporelle.
     */
    public double getTemporalDropRate(){
        return this.temporalDropRate;
    }

    /**
     * Accesseur pour le taux de décote liée au kilométrage.
     * @return Le taux de décote liée au kilométrage.
     */
    public double getMileageDropRate(){
        return this.mileageDropRate;
    }

    /**
     * Mutateur pour la marque de la voiture.
     * @param brand La nouvelle marque de la voiture.
     */
    public void setBrand(String brand){
        this.brand = brand;
    }

    /**
     * Mutateur pour la date de mise en circulation de la voiture.
     * @param onRoad La nouvelle date de mise en circulation de la voiture.
     */
    public void setOnRoad(LocalDate onRoad){
        this.onRoad = onRoad;
    }

    /**
     * Mutateur pour le prix d'achat de la voiture.
     * @param purchasePrice Le nouveau prix d'achat de la voiture.
     */
    public void setPurchasePrice(double purchasePrice){
        this.purchasePrice = purchasePrice;
    }

    /**
     * Mutateur pour la date de mise en vente de la voiture.
     * @param onSale La nouvelle date de mise en vente de la voiture.
     */
    public void setOnSale(LocalDate onSale){
        this.onSale = onSale;
    }

    /**
     * Mutateur pour le kilométrage de la voiture.
     * @param mileage Le nouveau kilométrage de la voiture.
     */
    public void setMileage(int mileage){
        this.mileage = mileage;
    }

    /**
     * Mutateur pour le taux de décote temporelle.
     * @param temporalDropRate Le nouveau taux de décote temporelle.
     */
    public void setTemporalDropRate(double temporalDropRate){
        this.temporalDropRate = temporalDropRate;
    }

    /**
     * Mutateur pour le taux de décote liée au kilométrage.
     * @param mileageDropRate Le nouveau taux de décote liée au kilométrage.
     */
    public void setMilenageDropRate(double mileageDropRate){
        this.mileageDropRate = mileageDropRate;
    }

    /**
     * Mutateur pour le prix de vente de la voiture.
     * @param salePrice Le nouveau prix de vente de la voiture.
     */
    private void setSalePrice(double salePrice){
        if(salePrice > 0){
            this.salePrice = salePrice;
        }
        else{
            this.salePrice = 0;
        }
    }

    /**
     * Méthode pour calculer le taux de décote total de la voiture.
     * @return Le taux de décote total de la voiture.
     */
    public double computeDropRate(){
        return  (getTemporalDropRate() * (onRoad.until(LocalDate.now(), ChronoUnit.MONTHS))) + (getMileageDropRate() * (mileage / 1000));
    }

    /**
     * Méthode pour estimer le prix de vente de la voiture.
     */
    public void priceEstimation(){
        setSalePrice(getPurchasePrice() * (1 - computeDropRate()));
    }

    /**
     * Accesseur pour le prix de vente de la voiture.
     * @return Le prix de vente estimé de la voiture.
     */
    public double getSalePrice(){
        priceEstimation();
        return this.salePrice;
    }

    /**
     * Méthode de représentation textuelle de la voiture.
     * @return Une représentation textuelle de la voiture.
     */
    public String toString() {
        return "Car [" + getBrand() + "," + getOnRoad() + "," + getMileage() + "km," + getSalePrice() + "euros]";  
    }
}

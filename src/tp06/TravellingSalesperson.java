package tp06;

import java.time.LocalDate;

/**
 * @author Hugo D
 * Classe représentant un voyageur de commerce.
 */
public class TravellingSalesperson extends Salesperson {
    /**
     * Pourcentage de commission sur le chiffre d'affaires.
     */
    private final double PERCENTAGE = 0.2;
    /**
     * Bonus fixe.
     */
    private final int BONUS = 400;

    /**
     * Constructeur de la classe TravellingSalesperson.
     * @param name Le nom du voyageur de commerce.
     * @param hiringDate La date d'embauche du voyageur de commerce.
     * @param turnover Le chiffre d'affaires réalisé par le voyageur de commerce.
     */
    public TravellingSalesperson(String name, LocalDate hiringDate, double turnover){
        super(name, hiringDate, turnover);
    }

    /**
     * Méthode pour obtenir le titre du voyageur de commerce.
     * @return Le titre du voyageur de commerce.
     */
    public String getTitle(){
        return "Voyageur";
    }

    /**
     * Méthode pour obtenir le salaire du voyageur de commerce.
     * @return Le salaire du voyageur de commerce.
     */
    public double getWages(){       
        return this.getTurnover() + BONUS + ((this.getTurnover()*PERCENTAGE)/100);
    }

    /**
     * Méthode pour vérifier si l'objectif du voyageur de commerce est atteint.
     * @return true si l'objectif est atteint, sinon false.
     */
    public boolean objectiveFulfilled(){
        return getWages() > OBJECTIVE;
    }
}

package tp06;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

/**
 * @author Hugo D
 * Classe représentant une concession automobile.
 */
public class Dealership {
    /**
     * Liste des voitures en vente dans la concession.
     */
    private ArrayList<Car> autos ;

    /**
     * Constructeur par défaut de la concession.
     */
    public Dealership() {
        this(new ArrayList<Car>());
    }

    /**
     * Constructeur avec initialisation de la liste des voitures.
     * @param autos La liste des voitures à inclure dans la concession.
     */
    public Dealership(ArrayList<Car> autos) {
        this.autos = autos;
    }

    /**
     * Méthode pour ajouter une voiture à la concession.
     * @param c La voiture à ajouter.
     */
    public void addCar(Car c) {
        this.autos.add(c);
    }

    /**
     * Méthode pour créer une voiture en spécifiant seulement la marque, la date de mise en circulation et le prix d'achat.
     * @param brand La marque de la voiture.
     * @param onRoad La date de mise en circulation de la voiture.
     * @param pPrice Le prix d'achat de la voiture.
     * @param km Le kilométrage de la voiture.
     * @return La voiture créée.
     */
    public Car createCar(String brand, LocalDate onRoad, double pPrice, int km) {
        return createCar(brand, onRoad, pPrice, LocalDate.now(), pPrice, km);
    }

    /**
     * Méthode pour créer une voiture en spécifiant toutes ses caractéristiques.
     * @param brand La marque de la voiture.
     * @param onRoad La date de mise en circulation de la voiture.
     * @param pPrice Le prix d'achat de la voiture.
     * @param onSale La date de mise en vente de la voiture.
     * @param sPrice Le prix de vente de la voiture.
     * @param km Le kilométrage de la voiture.
     * @return La voiture créée.
     */
    public Car createCar(String brand,LocalDate onRoad,double pPrice, LocalDate onSale,double sPrice,int km) {
        // Si la voiture a moins de 40 ans, elle est considérée comme normale, sinon elle est une voiture de collection
        if(onRoad.until(LocalDate.now(), ChronoUnit.YEARS) < 40) {
            return new Car(brand, onRoad, pPrice, onSale, sPrice, km);
        }
        else {
            return new VintageCar(brand, onRoad, pPrice, onSale, sPrice, km)  ;
        }
    }
}

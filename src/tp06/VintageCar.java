package tp06;

import java.time.LocalDate;

/**
 * @author Hugo D
 * Classe représentant une voiture de collection.
 */
public class VintageCar extends Car {

    /**
     * Constructeur avec toutes les informations sur la voiture de collection.
     * @param brand La marque de la voiture.
     * @param onRoad La date de mise en circulation de la voiture.
     * @param pPrice Le prix d'achat de la voiture.
     * @param onSale La date de mise en vente de la voiture.
     * @param sPrice Le prix de vente de la voiture.
     * @param km Le kilométrage de la voiture.
     */
    VintageCar(String brand, LocalDate onRoad, double pPrice, LocalDate onSale, double sPrice, int km) {
        super(brand, onRoad, pPrice, onSale, sPrice, km);
    }

    /**
     * Constructeur sans spécifier la date de mise en vente et le prix de vente de la voiture de collection.
     * @param brand La marque de la voiture.
     * @param onRoad La date de mise en circulation de la voiture.
     * @param pPrice Le prix d'achat de la voiture.
     * @param km Le kilométrage de la voiture.
     */
    VintageCar(String brand, LocalDate onRoad, double pPrice, int km) {
        super(brand, onRoad, pPrice, km);
    }

    /**
     * Accesseur pour le taux de décote temporelle de la voiture de collection.
     * @return Le taux de décote temporelle de la voiture de collection.
     */
    public double getTemporalDropRate(){
        return this.temporalDropRate;
    }

    /**
     * Accesseur pour le taux de décote liée au kilométrage de la voiture de collection.
     * @return Le taux de décote liée au kilométrage de la voiture de collection.
     */
    public double getMileageDropRate() {
        return this.mileageDropRate;
    }
}

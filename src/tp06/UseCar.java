package tp06;

import java.time.LocalDate;

/**
 * @author Hugo D
 * Classe de test pour les classes Car et VintageCar.
 */
public class UseCar {
    
    public static void main(String[] args) {
        // Création d'une voiture normale
        Car car = new Car("Renault", LocalDate.now().minusMonths(50), 10000.0, 20000);
        // Création d'une voiture de collection
        VintageCar vintageCar = new VintageCar("Renault", LocalDate.now().minusMonths(50), 10000.0, 20000);
        
        // Affichage des informations des voitures
        System.out.println(car);
        System.out.println(vintageCar);
    }
}

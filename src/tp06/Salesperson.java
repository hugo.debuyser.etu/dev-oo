package tp06;

import java.time.LocalDate;

/**
 * Classe abstraite représentant un vendeur.
 * @author Hugo D
 */
public abstract class Salesperson extends Employee {
    /**
     * Chiffre d'affaires réalisé par le vendeur.
     */
    private double turnover;

    /**
     * Constructeur de la classe Salesperson.
     * @param name Le nom du vendeur.
     * @param hiringDate La date d'embauche du vendeur.
     * @param turnover Le chiffre d'affaires réalisé par le vendeur.
     */
    public Salesperson(String name, LocalDate hiringDate, double turnover){
        super(name, hiringDate);
        this.turnover = turnover;
    }

    /**
     * Accesseur pour le chiffre d'affaires réalisé par le vendeur.
     * @return Le chiffre d'affaires réalisé par le vendeur.
     */
    public double getTurnover(){
        return this.turnover;
    }

    /**
     * Méthode de représentation textuelle du vendeur.
     * @return Une représentation textuelle du vendeur.
     */
    @Override
    public String toString(){
        return this.getTitle() + " " + this.getName();
    }

}

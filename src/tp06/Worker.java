package tp06;

import java.time.LocalDate;

/**
 * @auhtor Hugo D
 * Classe représentant un employé travaillant à la production.
 */
public class Worker extends Employee {
    /**
     * Rémunération par unité produite.
     */
    private final static double BY_UNIT = 5;
    /**
     * Objectif de production.
     */
    private final static int OBJECTIVE = 1000;
    /**
     * Nombre d'unités produites par l'employé.
     */
    private int units;

    /**
     * Constructeur de la classe Worker.
     * @param name Le nom de l'employé.
     * @param hiringDate La date d'embauche de l'employé.
     * @param units Le nombre d'unités produites par l'employé.
     */
    public Worker(String name, LocalDate hiringDate, int units){
        super(name, hiringDate);
        this.units = units;
    }

    /**
     * Méthode pour obtenir le titre de l'employé.
     * @return Le titre de l'employé.
     */
    public String getTitle(){
        return "Employée";
    }

    /**
     * Méthode pour obtenir le salaire de l'employé.
     * @return Le salaire de l'employé.
     */
    public double getWages(){
        return BY_UNIT * ((double) units);
    }

    /**
     * Méthode de représentation textuelle de l'employé.
     * @return Une représentation textuelle de l'employé.
     */
    @Override
    public String toString(){
        return this.getTitle() + " " + this.getName();
    }

    /**
     * Méthode pour vérifier si l'objectif de production de l'employé est atteint.
     * @return true si l'objectif est atteint, sinon false.
     */
    public boolean objectiveFulfilled(){
        return getWages() >= OBJECTIVE;
    }
}

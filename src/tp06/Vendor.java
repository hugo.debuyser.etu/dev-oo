package tp06;

import java.time.LocalDate;

/**
 * @author Hugo D
 * Classe représentant un vendeur.
 */
public class Vendor extends Salesperson {
    /**
     * Pourcentage de commission sur le chiffre d'affaires.
     */
    private final double PERCENTAGE = 0.2;
    /**
     * Bonus fixe.
     */
    private final int BONUS = 800;

    /**
     * Constructeur de la classe Vendor.
     * @param name Le nom du vendeur.
     * @param hiringDate La date d'embauche du vendeur.
     * @param turnover Le chiffre d'affaires réalisé par le vendeur.
     */
    public Vendor(String name, LocalDate hiringDate, double turnover){
        super(name, hiringDate, turnover);
    }

    /**
     * Méthode pour obtenir le titre du vendeur.
     * @return Le titre du vendeur.
     */
    public String getTitle(){
        return "Vendeur";
    }

    /**
     * Méthode pour obtenir le salaire du vendeur.
     * @return Le salaire du vendeur.
     */
    public double getWages(){
        return this.getTurnover() + BONUS + ((this.getTurnover()*PERCENTAGE)/100);
    }
    
    /**
     * Méthode pour vérifier si l'objectif du vendeur est atteint.
     * @return true si l'objectif est atteint, sinon false.
     */
    public boolean objectiveFulfilled(){
        return getWages() > OBJECTIVE;
    }

}

package tp06;

import java.time.LocalDate;

/**
 * @author Hugo D
 * Classe abstraite représentant un employé.
 */
public abstract class Employee {
    /**
     * Nom de l'employé.
     */
    private String name;
    /**
     * Date d'embauche de l'employé.
     */
    private LocalDate hiringDate;
    /**
     * Objectif fixé pour l'employé.
     */
    protected static final double OBJECTIVE = 10000.0;

    /**
     * Constructeur de la classe Employee.
     * @param name Le nom de l'employé.
     * @param hiringDate La date d'embauche de l'employé.
     */
    public Employee(String name, LocalDate hiringDate){
        this.name = name;
        this.hiringDate = hiringDate;
    }

    /**
     * Accesseur pour le nom de l'employé.
     * @return Le nom de l'employé.
     */
    public String getName() {
        return name;
    }

    /**
     * Accesseur pour la date d'embauche de l'employé.
     * @return La date d'embauche de l'employé.
     */
    public LocalDate getHiringDate() {
        return hiringDate;
    }

    /**
     * Mutateur pour le nom de l'employé.
     * @param name Le nouveau nom de l'employé.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Mutateur pour la date d'embauche de l'employé.
     * @param hiringDate La nouvelle date d'embauche de l'employé.
     */
    public void setHiringDate(LocalDate hiringDate) {
        this.hiringDate = hiringDate;
    }

    /**
     * Méthode abstraite pour obtenir le titre de l'employé.
     * @return Le titre de l'employé.
     */
    public abstract String getTitle();

    /**
     * Méthode abstraite pour obtenir le salaire de l'employé.
     * @return Le salaire de l'employé.
     */
    public abstract double getWages();

    /**
     * Méthode abstraite pour vérifier si l'objectif de l'employé est atteint.
     * @return true si l'objectif est atteint, sinon false.
     */
    public abstract boolean objectiveFulfilled();

    /**
     * Méthode pour représenter l'employé sous forme de texte.
     * @return Une représentation textuelle de l'employé.
     */
    @Override
    public String toString(){
        return this.getTitle() + " "+ this.getName();
    }

}

import java.util.ArrayList;

/**
 * Classe pour agenda de tâche.
 * @author Hugo D
 */
public class Agenda {
    /** * Liste d'évènement. */
    public ArrayList<Event> events;

    /**
     * Constructeur avec une liste d'évènements.
     * @param events */
    public Agenda(ArrayList<Event> events){
        this.events = events;
    }

    /** * Construteur sans paramètre. */
    public Agenda(){
        this(new ArrayList<Event>());
    }

    /**
     * Getteur qui renvoie la liste d'évènement de l'agenda.
     * @return Renvoie une ArrayList de type Event des évènements de l'agenda.
     */
    public ArrayList<Event> getEvents() {
        return events;
    }

    /**
     * Renvoie les attributs de la classe Agenda.
     * @return Affiche la liste des évènements.
     */
    public String toString(){
        return events.toString();
    }

    /**
     * Détermine si un évènement passé en paramètre ne chevauche aucun évènement déjà présent dans l’agenda.
     * @param event Évènement passé en paramètre.
     * @return Renvoie un booléen qui détermine si l'événment en paramètre est déjà présent ou non.
     */
    public boolean conflicting(Event event){
        boolean chevauche = false;
        int idx = 0;
        while(chevauche == false && idx <events.size()){
            if(events.get(idx).overlap(event)){
                chevauche = !chevauche;
            }
            ++idx;
        }
        return chevauche;
    }

    /**
     * Ajoute un évènement à l'agenda avec un évènement en paramètre.
     * @param evt Évènement à ajouter.
     */
    public void addEvent(Event evt) {
        if (!conflicting(evt)) {
            int idx = 0;
            while (idx < events.size() && events.get(idx).getStart().isBefore(evt.getStart())) {
                idx++;
            }
            events.add(idx, evt);
            sortEvents();
        }
    }
    
    /**
     * Ajouter un évènement ainsi que la position dans l'agenda.
     * @param evt Évènement à ajouter.
     * @param idx Indice de l'agenda.
     */
    public void addEvent(Event evt, int idx) {
        if (!conflicting(evt) && idx >= 0 && idx <= events.size()) {
            events.add(idx, evt);
            sortEvents();
        }
    }
    
    /**
     * Retire un évènement donné de l'agenda.
     * @param evt Évènement à retirer.
     */
    public void removeEvent(Event evt) {
        removeEvent(events.indexOf(evt));
    }

    /**
     * Retire l'évènement à un certain indice de l'agenda.
     * @param idx Indice de l'évènement à retirer.
     */
    public void removeEvent(int idx) {
        if (idx >= 0 && idx < events.size()) {
            events.remove(idx);
        }
    }

    /**
     * Retire un évènement de l'agenda celon son label.
     * @param label Label de l'évènement à retirer.
     */
    public void removeEvent(String label) {
        boolean trouve = false;
        int idx = 0;
        while (!trouve && idx < events.size()) {
            if(events.get(idx).getLabel().equals(label)) {
                removeEvent(idx);
                trouve = true;
            }            
            idx++;
        }
    }

    /**
     * Supprime tous les évènements de l’agenda qui chevauchent celui fourni en paramètre.
     * @param evt Évènement fourni en paramètre.
     */
    public void removeOverlapping(Event evt) {
        if(conflicting(evt)==true) {
            for(int idx = 0; idx<events.size(); idx++) {
                if(events.get(idx).overlap(evt)) {
                    removeEvent(idx);
                }
            }
        }
    }

    /**
     * Supprime tous les évènements entre ceux fournis en paramètres, c’est-à dire ceux chevauchant la période comprise entre le début du premier et la fin du second.
     * @param evt1 Premier évènement.
     * @param evt2 Deuxième évènement.
     */
    public void removeBetween(Event evt1, Event evt2) {
        Event evt3 = new Event(null, null, evt1.getStart(), evt2.getEnd());
        removeOverlapping(evt3);
    }

    /**
     * Garantie que l'agenda reste toujours trié par ordre croissant des dates de début d’évènement.
     */
    private void sortEvents() {
        boolean swapped;
        do {
            swapped = false;
            for (int i = 1; i < events.size(); i++) {
                if (events.get(i - 1).getStart().isAfter(events.get(i).getStart())) {
                    Event temp = events.get(i - 1);
                    events.set(i - 1, events.get(i));
                    events.set(i, temp);
                    swapped = true;
                }
            }
        } while (swapped);
    }

    /**
     * Réinitialise l'agenda.
     */
    public void clear() {
        this.events = new ArrayList<Event>();
    }
    
}

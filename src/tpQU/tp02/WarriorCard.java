/**
 * Classe permettant de créer une carte WarriorCard avec les différents attributs.
 * @author Hugo D */
public class WarriorCard {
    //attributes
    /**
     * Attribut pour le nom de la carte. */
    private String name;

    /**
     * Attribut pour la valeur "force" de la carte. */
    private int strength;

    /**
     * Attribut pour la valeur "agilité" de la carte. */
    private int agility;

    //constructor

    /**
     * Constructeur principale de la classe WarriorCard.
     * @param name nom de la carte.
     * @param s Attribut "froce" de la carte.
     * @param ag Attribut "agilité" de la carte. */
    public WarriorCard(String name, int s, int ag){
        this.name = name;
        this.strength = s;
        this.agility = ag; 
    }

    /**
     * getteur pour l'attribut nom de la carte
     * @return renvoie un String de l'attribut nom de la carte courante. */
    public String getName(){
        return this.name;
    }

    /**
     * getteur pour l'attribut "force" de la carte".
     * @return renvoie un int de l'attribut "force" de la carte courante. */
    public int getStrength(){
        return this.strength;
    }

    /**
     * getteur pour l'attribut "agilité" de la carte.
     * @return renvoie un int de l'attribut "agilité" de la carte courante. */
    public int getAgility(){
        return this.agility;
    }

    /**
     * setteur pout l'attribut nom de la carte.
     * @param name Nouveau nom à attribuer à la carte courante. */
    public void setNom(String name){
        this.name = name;
    }

    /**
     * setteur pour l'attribut "strength" de la carte.
     * @param strength Nouvelle valeur pour l'attribut "force" de la carte courante. */
    public void setStrength(int strength){
        this.strength = strength;
    }

    /**
     * setteur pout l'attribut "agility" de la carte.
     * @param agility Nouvelle valeur pour l'attribut "agility" de la carte courante. */
    public void setAgility(int agility){
        this.agility = agility;
    }

    /**
     * Vérifie si un objet WarriorCard est égal à un autre.
     * @param obj Autre carte passé en paramètre.
     * @return renvoie True si les deux cartes sont identiques, sinon non. */
    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null || getClass() != obj.getClass()) return false;
        WarriorCard other = (WarriorCard) obj;
        if(strength != other.strength) return false;
        if(agility != other.agility) return false;
        if (name == null){
            if(other.name != null) return false;
        } else if(!name.equals(other.name)) return false;
        return true;
    }
    /**
     * Compare l'attribut "Force" de 2 cartes.
     * @param other Autre carte passé en paramètre.
     * @return renvoie -1 si la carte passé en paramètre à une valeur de "force" est supérieur à celle courante, 1 si inférieur et 0 si identique.*/
    public int compareStrength(WarriorCard other){
        if(this.strength<other.strength) return -1;
        if(this.strength>other.strength) return 1;
        return 0;
    }

    /**
     * Compare l'attribut "Agilité" de 2 cartes.
     * @param other Autre carte passé en paramètre.
     * @return renvoie -1 si la carte passé en paramètre à une valeur de "agilité" est supérieur à celle courante, 1 si inférieur et 0 si identique.*/
    public int compareAgility(WarriorCard other){
        if(this.agility<other.agility) return -1;
        if(this.agility>other.agility) return 1;
        return 0;
    }

    /**
     * Renvoie la carte et ses valeur.
     * @return renvoie 'nom'[S='strength',A='agility'].*/
    public String toString(){
        return this.name + "[S=" + this.strength + ",A=" + this.agility + "]";
    }

}

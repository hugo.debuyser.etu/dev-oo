import tp03.Task;
import tp03.ToDoList;

import java.time.*;

/**
 * Un évènement est un fait qui survient à un moment donné, et pour une certaine durée.
 * @author Hugo D
 */
public class Event {
    /*** Attribut label pour la classe.*/
    private String label;
    /*** Attribut place pour la classe. */
    private String place;
    /***Attribut start pour la classe. */
    private LocalDate start;
    /***Attribut end pour la classe. */
    private LocalDate end;
    /***Liste de tache de type ToDoList. */
    private ToDoList tasks;

    /** 
     * Constructeur de la classe Event avec les attributs suivants :
     * @param label Attribut label de la classe.
     * @param place Attribut place de la classe.
     * @param start Attribut start de la classe.
     * @param end Attribut end de la classe seulement si elle est après start.
     * @param tasks Nouvelle tache assigné à la classe.
     */
    public Event(String label, String place, LocalDate start, LocalDate end, ToDoList tasks){
        this.label = label;
        this.place = place;
        if(start.isBefore(end)){
            this.start = start;
            this.end = end;
        }
        else{
            this.start = start;
            this.end = start;
        }
        this.tasks = tasks;
    }

    /**
     * Constructeur avec une liste nouvelle liste, avec les attributs suivants :
     * @param label Attribut label de la classe.
     * @param place Attribut place de la classe.
     * @param start Attribut start de la classe.
     * @param end Attribut end de la classe seulement si elle est après start.
     */
    public Event(String label, String place, LocalDate start, LocalDate end){
        this(label, place, start, end, new ToDoList());
    }

    /**
     * Constructeur avec une date de fin égal à la date du jour après la création, avec les attributs suivants :
     * @param label Attribut label de la classe.
     * @param place Attribut place de la classe.
     * @param start Attribut start de la classe.
     */
    public Event(String label, String place, LocalDate start){
        this(label, place, start, LocalDate.of(start.getYear(), start.getMonth(), start.getDayOfMonth()+1));
    }

    /**
     * Constructeur avec une date de début par défaut et les attributs suivants :
     * @param label Attribut label de la classe.
     * @param place Attribut place de la classe.
     */
    public Event(String label, String place){
        this(label, place ,LocalDate.now());
    }

    /**
     * Getteur pour le label de l'évènement courant.
     * @return Renvoie le label de l'évènement courant.
     */
    public String getLabel() {
        return label;
    }

    /**
     * Setteur pour modifier le label de l'évènement courant.
     * @param label Nouveau label pour l'évènement courant.
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Getteur pour la place de l'évènement courant.
     * @return Renvoie la place de l'évènement courant.
     */
    public String getPlace() {
        return place;
    }

    /**
     * Setteur pour modifier la place de l'évènement courant.
     * @param place Nouvelle place pour l'évènement courant.
     */
    public void setPlace(String place) {
        this.place = place;
    }

    /**
     * Getteur pour le début de l'évènement courant.
     * @return Renvoie le début de l'évènement courant.
     */
    public LocalDate getStart() {
        return start;
    }

    /**
     * Setteur pour modifier le début de l'évènement courant.
     * @param place Nouvelle date de début pour l'évènement courant.
     */
    public void setStart(LocalDate start) {
        this.start = start;
    }

    /**
     * Getteur pour la fin de l'évènement courant.
     * @return Renvoie la fin de l'évènement courant.
     */
    public LocalDate getEnd() {
        return end;
    }

    /**
     * Setteur pour modifier la fin de l'évènement courant.
     * @param place Nouvelle date de fin pour l'évènement courant.
     */
    public void setEnd(LocalDate end) {
        this.end = end;
    }

    /**
     * Getteur pour les tâches de l'évènement courant.
     * @return Renvoie les tâches de l'évènement courant.
     */
    public ToDoList getTasks() {
        return tasks;
    }

    /**
     * Setteur pour modifier les tâches de l'évènement courant.
     * @param place Nouvelle tâches pour l'évènement courant.
     */
    public void setTasks(ToDoList tasks) {
        this.tasks = tasks;
    }

     /**
     * Renvoie l'évènement et ses valeurs.
     * @return renvoie "label"-"place": "start"-"end"
     */
    @Override
    public String toString() {
        return  label + " - " + place + ": \t " + start.toString() + " -> " + end.toString();
    }

    /**
     * Ajoute une tâche à l'évènement.
     * @param aTask Nouvelle tâche à ajouter.
     */
    public void addTask(Task aTask) {
        tasks.addTask(aTask);
    }

    /**
     * Enlève une tâche de l'évènement courant.
     * @param aTask Tâche à retirer.
     */
    public void removeTask(Task aTask) {
        tasks.removeTask(aTask);
    }

    /**
     * Renvoie le nombre de tâches de l'évènement.
     * @return Renvoie l'entier du nombre de tâches.
     */
    public int getNbTasks() {
        return tasks.getNbTask();
    }

    /**
     * Renvoie les différentes tâches de l'évènement.
     * @return Renvoie une liste de tâches.
     */
    public Task[] getChores() {
        return tasks.getChores();
    }

    /**
     * Vérifie si un objet évènement est égal à un autre.
     * @param obj Autre évènement passé en paramètre.
     * @return renvoie True si les deux évènement sont identiques, sinon non. */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Event other = (Event) obj;
        if (label == null) {
            if (other.label != null) return false;
        } else if (!label.equals(other.label)) return false;
        if (place == null) {
            if (other.place != null) return false;
        } else if (!place.equals(other.place)) return false;
        if (start == null) {
            if (other.start != null) return false;
        } else if (!start.equals(other.start)) return false;
        if (end == null) {
            if (other.end != null) return false;
        } else if (!end.equals(other.end)) return false;
        return true;
    }

    /**
     *  Retourne true si une partie de l’évènement courant se déroule en même temps que celui passé en paramètre, et false sinon.
     * @param other Autre évènement passé en paramètre.
     * @return Renvoie un booléen celon si les évènements se déroulent en même temps ou non.
     */
    public boolean overlap(Event other){
        if(this.end.isBefore(other.start)){
            return false;}
        if(other.end.isBefore(this.start)){
            return false;}
        if(this.start.equals(other.end)){
            return false;}
        if(other.start.equals(this.end)){
            return false;}
        return true;
    }

}

package tpQU.tp03;

/**
 * Classe pour étudiant de première année.
 */
public class StudentBUT1 extends Student {
    /**
     * Constructeur avec les attributs suivants :
     * @param name Nom de l'étudiant.
     * @param forename Prénom de l'étudiant.
     */
    public StudentBUT1(String name, String forename) {
        super(name, forename);
    }

    /**
     * Constructeur avec les attributs suivants :
     * @param name Nom de l'étudiant.
     * @param forename Prénom de l'étudiant.
     * @param duration Durée de présentation de la soutenance.
     */
    public StudentBUT1(String name, String forename, int duration) {
        super(name, forename, duration);
    }
}

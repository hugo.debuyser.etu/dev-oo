package tpQU.tp03;

/**
 * Classe pour étudiant.
 */
public abstract class Student extends Person {
     /**
     * Valeur par défaut de durée.
     */
    public static final int DEFAULT_DURATION = 20;

    /**
     * Attribut de durée de la classe.
     */
    private int duration;

    /**
     * Constructeur avec les attributs suivants :
     * @param name Nom de l'étudiant.
     * @param forename Prénom de l'étudiant.
     */
    public Student(String name, String forename) {
        super(name, forename);
        this.duration = DEFAULT_DURATION;
    }

    /**
     * Constructeur avec les attributs suivants :
     * @param name Nom de l'étudiant.
     * @param forename Prénom de l'étudiant.
     * @param duration Durée de présentation de la soutenance.
     */
    public Student(String name, String forename, int duration) {
        super(name, forename);
        this.duration = duration;
    }

    /**
     * Getteur renvoyant la durée de l'objet.
     * @return Renvoie une chaine de caractère avec la durée.
     */
    public int getDuration() {
        return duration;
    }

    @Override
    public String toString() {
        return super.toString() + " (" + duration + ")";
    }
}


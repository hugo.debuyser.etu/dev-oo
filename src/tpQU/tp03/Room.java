package tpQU.tp03;

public enum Room {
    r0A18("room 0A18"),
    r0A20("room 0A20"),
    r0A51("room 0A51"),
    r0A49("room 0A49");

    private final String displayName;

    Room(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public String toString() {
        return displayName;
    }
}

package tpQU.tp03;

/**
 * Classe pour étudiant de deuxième année.
 */
public class Student2 extends Student1{
    /**
     * Valeur par défaut de durée.
     */
    public static final int DEFAULT_DURATION = 30;

    /**
     * Attribut de durée de la classe.
     */
    private int duration;

    /**
     * Constructeur avec les attributs suivants :
     * @param name Nom de l'étudiant.
     * @param forename Prénom de l'étudiant.
     */
    public Student2(String name, String forename){
        super(name, forename);
        this.duration = DEFAULT_DURATION;
    }

    /**
     * Constructeur avec les attributs suivants :
     * @param name Nom de l'étudiant.
     * @param forename Prénom de l'étudiant.
     * @param duration Durée de présentation de la soutenance.
     */
    public Student2(String name, String forename, int duration){
        this(name, forename);
        this.duration = duration;
    }

    @Override
    public String toString(){
        return super.toString() + "(" + this.duration + ")";
    }
}

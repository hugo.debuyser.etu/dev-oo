package tpQU.tp03;

import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Classe pour la défence.
 */
public class Defense {
    /**
     * Attribut pour la date.
     */
    private LocalDate whenDay;
    /**
     * Attribut pour l'endroit.
     */
    private Room where;
    /**
     * Attribut pour l'étudiant.
     */
    private Student who;
    /**
     * Attribut pour l'heure.
     */
    private LocalTime whenHour;
    /**
     * Attribut pour le titre.
     */
    private String title;

    /**
     * Constructeur avec les attributs suivants :
     * @param whenDay Date.
     * @param where L'endroit.
     * @param who Étudiant.
     * @param whenHour Heure.
     * @param title titre.
     */
    public Defense(LocalDate whenDay, Room where, Student who, LocalTime whenHour, String title){
        this (where, who, title);
        this.whenDay = whenDay;
        this.whenHour = whenHour;
    }

    /**
     * Constructeur avec les attributs suivants :
     * @param where L'endroit.
     * @param who Étudiant.
     * @param title titre.
     */
    public Defense(Room where, Student who, String title){
        this.where = where;
        this.who = who;
        this.title = title;
    }

    @Override
    public String toString() {
        return this.whenDay + ":" + this.whenHour + " in " + this.where + " --> " + this.who + " :: " + this.title;
    }

    /**
     * Getteur pour le jour.
     * @return Renvoie une jour.
     */
    public LocalDate getDay(){
        return this.whenDay;
    }

    /**
     * Setteur pour le jour.
     * @param aDate Jour à attribuer.
     */
    public void setDay(LocalDate aDate){
        this.whenDay = aDate;
    }
    
    /**
     * Getteur de l'heure.
     * @return Renvoie une heure.
     */
    public LocalTime getHour(){
        return this.whenHour;
    }

    /**
     * Setteur pour l'heure.
     * @param aTime Heure à attribuer.
     */
    public void setHour(LocalTime aTime){
        this.whenHour = aTime;
    }

    /**
     * Getteur pour le titre.
     * @return Renvoie une chaine de caractère du titre.
     */
    public String getTitle(){
        return this.title;
    }

    /**
     * Getteur pour l'endroit.
     * @return Renvoie l'endroit avec le type Room.
     */
    public Room getPlace(){
        return this.where;
    }

    /**
     * Getteur pour l'étudiant.
     * @return Renvoie l'étudiant.
     */
    public Student getStudent(){
        return this.who;
    }

    /**
     * Getteur pour la durée associé à l'étudiant.
     * @return renvoie un entier associer à la durée.
     */
    public int getDuration() {
        return who.getDuration();
    }
     
}

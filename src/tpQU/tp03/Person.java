package tpQU.tp03;

/**
 * @author Hugo D
 * Classe personne.
 */
public class Person{
    /**
     * Attribut nom de type String.
     */
    private String name;
    /**
     * Attribut prénom de type String.
     */
    private String forename;

    /**
     * Constructeur principale pour la classe.
     * @param name Nom à ajouter à l'objet.
     * @param forename Prénom à ajouter à l'objet.
     */
    public Person(String name, String forename){
        this.name = name;
        this.forename = forename;
    }

    /**
     * Getteur renvoyant le nom de l'objet.
     * @return Renvoie une chaine de caractère avec le nom.
     */
    public String getName(){
        return this.name;
    }

    /**
     * Setteur pour attribuer le nom à l'objet.
     * @param name Nom à ajouter à l'objet.
     */
    public void setName(String name){
        this.name = name;
    }
    
    /**
     * Getteur renvoyant le Prénom de l'objet.
     * @return Renvoie une chaine de caractère avec le prénom.
     */
    public String getForename(){
        return this.forename;
    }

    /**
     * Setteur renvoyant le Prénom.
     * @param forename Prénom à ajouter à l'objet.
     */
    public void setForname(String forename){
        this.forename = forename;
    }

    @Override
    /**
     * Renvoie une chaine de caractère avec les différents attributs de la classe.
     * @return Renvoie la chaine de caractère.
     */
    public String toString(){
        return this.forename + " " + this.name;
    }

}
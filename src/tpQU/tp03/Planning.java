package tpQU.tp03;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe pour la gestion des plannings des défenses.
 */
public class Planning {
    private LocalDate when;
    public List<Defense> planning;
    public static LocalTime defaultStart = LocalTime.of(8, 0, 0);
    public static int breakDuration = 5;

    /**
     * Constructeur pour initialiser un planning avec une date spécifique.
     * @param when La date du planning.
     */
    public Planning(LocalDate when){
        this.when = when;
        this.planning = new ArrayList<>();
    }

    /**
     * Getteur pour obtenir la date du planning.
     * @return La date du planning.
     */
    public LocalDate getDay(){
        return this.when;
    }

    /**
     * Setter pour modifier la date du planning.
     * @param when La nouvelle date du planning.
     */
    public void setDay(LocalDate when){
        this.when = when;
    }

    /**
     * Ajoute une défense au planning.
     * @param defense La défense à ajouter.
     */
    public void addDefense(Defense defense) {
        this.planning.add(defense);
    }

    /**
     * Ajoute plusieurs défenses au planning.
     * @param d Liste des défenses à ajouter.
     */
    public void addDefense(ArrayList<Defense> d) {
        this.planning.addAll(d);
    }

    /**
     * Obtient une défense par son index dans le planning.
     * @param index L'index de la défense.
     * @return La défense à l'index spécifié.
     */
    public Defense getDefense(int index) {
        return planning.get(index);
    }

    /**
     * Obtient l'index d'une défense dans le planning.
     * @param d La défense à rechercher.
     * @return L'index de la défense, ou -1 si la défense n'est pas trouvée.
     */
    public int getDefense(Defense d) {
        for (int idx = 0; idx < this.planning.size(); idx++) {
            if (this.planning.get(idx).equals(d)) {
                return idx;
            }
        }
        return -1;
    }
    

    /**
     * Obtient le nombre total de défenses dans le planning.
     * @return Le nombre de défenses.
     */
    public int getNbDefense() {
        return planning.size();
    }

    /**
     * Planifie les défenses à partir d'une heure de début spécifiée.
     * @param start L'heure de début des défenses.
     */
    public void scheduling(LocalTime start) {
        LocalTime currentTime = start;
        for(int idx = 0; idx<this.planning.size(); idx++) {
            this.planning.get(idx).setDay(this.when);
            this.planning.get(idx).setHour(currentTime);
            int plus = (breakDuration + this.planning.get(idx).getDuration());
            currentTime = currentTime.plusMinutes(plus);
            System.out.println(currentTime);
        }
    }

    /**
     * Planifie les défenses à partir de l'heure de début par défaut.
     */
    public void scheduling() {
        scheduling(Planning.defaultStart);
    }

    /**
     * Planifie les défenses à partir d'une heure de début spécifiée et à partir d'un index spécifique.
     * @param start L'heure de début des défenses.
     * @param indiceDepart L'index à partir duquel commencer le planification.
     */
    void scheduling(LocalTime start, int indiceDepart) {
        LocalTime time = start;
        for(int idx = 0; idx<this.planning.size(); idx++) {
            this.planning.get(idx).setDay(this.when);
            this.planning.get(idx).setHour(LocalTime.of(time.getHour(), time.getMinute()));
            int plus = (breakDuration + this.planning.get(idx).getDuration());
            time = time.plusMinutes(plus);
            System.out.println(time);
        }
    }

    /**
     * Échange les heures de deux défenses dans le planning.
     * @param d1 La première défense.
     * @param d2 La deuxième défense.
     */
    public void swapDefense(Defense d1, Defense d2) {
        LocalTime time1 = d1.getHour();
        d1.setHour(d2.getHour());
        d2.setHour(time1);
        int idx1 = getDefense(d1);
        this.planning.set(getDefense(d2), d1);
        this.planning.set(idx1, d2);
    }

    /**
     * Retarde la défense spécifiée d'un certain nombre de minutes.
     * @param d1 La défense à retarder.
     * @param minutesToAdd Le nombre de minutes à ajouter.
     */
    public void delayDefense(Defense d1, int minutesToAdd) {
        int index = getDefense(d1);
        if (index != -1) {
            LocalTime newStartTime = d1.getHour().plusMinutes(minutesToAdd);
            for (int i = index; i < planning.size(); i++) {
                LocalTime adjustedTime = planning.get(i).getHour().plusMinutes(minutesToAdd);
                planning.get(i).setHour(adjustedTime);
            }
            scheduling(newStartTime, index);
        }
    }
    
}

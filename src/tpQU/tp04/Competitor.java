package tpQU.tp04;

public class Competitor {
    private static int compt = 0;

    private final String NAME;
    private Country nationality;
    private Team team;
    private final int ID;

    public Competitor(String name, Country nationality, Team team) {
        this.NAME = name;
        this.nationality = nationality;
        this.team = team;
        this.ID = compt++;
    }

    public String getName(){
        return this.NAME;
    }
    public int getID(){
        return this.ID;
    }
    public Team getTeam(){
        return this.team;
    }
    public Country getNationality(){
        return this.nationality;
    }

    public boolean isFrom(Country country){
        return(this.nationality == country);
    }

    public boolean isFrom(Team team){
        return(this.team == team);
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(this.getClass() != obj.getClass()) return false;
        Competitor other = (Competitor) obj;
        if(!this.NAME.equals(other.getName()))return false;
        if(this.ID != other.ID)return false;
        return true;
    }

    public static void resetCpt(){
        compt = 0;
    }

    @Override
    public String toString() {
        return this.ID + "-" + this.NAME + "(" + this.nationality.getNAME() + ") -> " + this.team;       
    }
}

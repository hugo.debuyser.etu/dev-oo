package tpQU.tp04;

public class SkiingStage extends Stage {
    private final int distance;

    public SkiingStage(int distance) {
        this.distance = distance;
    }

    public Integer getLength() {
        return distance;
    }

    @Override
    public String getType() {
        return "Skiing";
    }

    @Override
    public String toString() {
        return this.getID() + " (" + this.getClass().getName() + ")" + this.distance + "m";
    }
}

package tpQU.tp04;

public class ExtendedException extends Exception {
    public ExtendedException(String name){
        super(name);
    }
}

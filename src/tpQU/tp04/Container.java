package tpQU.tp04;

public enum Container {
    BLISTER(1),
    BOX(10),
    CRATE(50);

    private final int capacity;

    private Container(int capacity){
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

}
package tpQU.tp04;

public class ShootingStage extends Stage{
    private final int nbShots;
    private final int penaltyPerMiss;

    public ShootingStage(int nbShots, int penaltyPerMiss) {
        this.nbShots = nbShots;
        this.penaltyPerMiss = penaltyPerMiss;
    }

    public ShootingStage(int nbShots) {
        this(nbShots, 10);
    }

    public Integer getNbShots() {
        return nbShots;
    }

    public int getPenaltyPerMiss() {
        return penaltyPerMiss;
    }

    @Override
    public String getType() {
        return "Shooting";
    }

    @Override
    public String toString() {
        return this.getID() + " (" + this.getClass().getName() + ")" + this.nbShots + "shots/penaltyPerMiss=" + this.penaltyPerMiss;
    }
}

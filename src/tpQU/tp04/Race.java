package tpQU.tp04;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;

public class Race {
    private final String NAME;
    private Set<Stage> stages;
    private Map<Integer, Competitor> competitors;

    public Race(String name){
        this.NAME = name;
        this.stages = new HashSet<>();
        this.competitors = new HashMap<>();
    }

    public void addStage(Stage aStage){
        this.stages.add(aStage);
    }

    public Stage getStage(int id){
        for(Stage stage : stages){
            if(stage.getID() == id){
                return stage;
            }
        }
        return null;
    }

    public void addCompetitor(Competitor aCompetitor){
        if(competitors.containsValue(aCompetitor)){
            return;
        }
        competitors.put(aCompetitor.getID(), aCompetitor);
    }

    public Competitor getCompetitor(int id) throws Exception_UnknownCompetitor{
        if(competitors.containsKey(id)){
            return competitors.get(id);
        }
        throw new Exception_UnknownCompetitor("No competitor found for this id");
    }

    public int getNbCompetitors(){
        return competitors.size();
    }

    public int getNbTeams(){
        List<Team> teams = new ArrayList<>();
        for(Competitor competitor : competitors.values()){
            if(teams.contains(competitor.getTeam())){
                teams.add(competitor.getTeam());
            }
        }
        return teams.size();
    }

    public int getNbCountries(){
        List<Country> nbCountries = new ArrayList<>();
        for(Competitor competitor : competitors.values()){
            if(nbCountries.contains(competitor.getNationality())){
                nbCountries.add(competitor.getNationality());
            }
        }
        return nbCountries.size();
    }

    public int getTotalNbShots(){
        int nbShots = 0;
        for(Stage stage : stages){
            if (stage instanceof ShootingStage) {
                nbShots += ((ShootingStage) stage).getNbShots();
            }
        }
        return nbShots;
    }

    public int getTotalLength(){
        int totalLength = 0;
        for(Stage stage : stages){
            if (stage instanceof SkiingStage) {
                totalLength += ((SkiingStage) stage).getLength();;
            }
        }
        return totalLength;
    }

    public void clearResult(){
        for(Stage stage : stages){
            stage.clear();
        }
    }

    public void isValid(Competitor c) throws Exception_UnknownCompetitor{
        if(!competitors.containsValue(c)){
            throw new Exception_UnknownCompetitor("Competitor not found in the list");
        }
    }

    public void isValid(int id) throws Exception_UnknownCompetitor{
        if(!competitors.containsKey(id)){
            throw new Exception_UnknownCompetitor("Competitor not found with this id");
        }
    }

    public void record(Competitor c, Stage s, int value) throws Exception_InvalidRecord, Exception_UnknownCompetitor {
        isValid(c);
        s.record(c, value);
    }

    public int getScore(Competitor aCompetitor, Stage aStage) throws Exception_NoResult, Exception_UnknownCompetitor {
        isValid(aCompetitor);
        return aStage.getScore(aCompetitor);
    }

    public int getScore(int competitorID, int stageID) throws Exception_NoResult, Exception_UnknownCompetitor {
        Competitor competitor = getCompetitor(competitorID);
        Stage stage = getStage(stageID);
        if (stage == null) {
            throw new Exception_UnknownCompetitor("Stage not found.");
        }
        return stage.getScore(competitor);
    }

    public int getCompetitorScore(Competitor c) throws Exception_NoResult, Exception_UnknownCompetitor {
        isValid(c);
        int totalScore = 0;
        for (Stage stage : stages) {
            totalScore += stage.getScore(c);
        }
        return totalScore;
    }

    public double getTeamScore(Team team) throws Exception_NoResult, Exception_UnknownCompetitor{
        List<Competitor> teamMembers = new ArrayList<>();
        for (Competitor competitor : competitors.values()) {
            if (competitor.getTeam() == team) {
                teamMembers.add(competitor);
            }
        }
        int totalScore = 0;
        for (Competitor member : teamMembers) {
            totalScore += getCompetitorScore(member);
        }
        if(teamMembers.isEmpty()){
            return 0;
        }
        return ((double) totalScore / teamMembers.size());
    }

    public double getCountryScore(Country country) throws Exception_NoResult, Exception_UnknownCompetitor {
        List<Competitor> countryMembers = new ArrayList<>();
        for (Competitor competitor : competitors.values()) {
            if (competitor.getNationality() == country) {
                countryMembers.add(competitor);
            }
        }
        int totalScore = 0;
        for (Competitor member : countryMembers) {
            totalScore += getCompetitorScore(member);
        }
        if(countryMembers.isEmpty()){
            return 0;
        }
        return ((double) totalScore / countryMembers.size());
    }

    public List<Competitor> competitorRanking(){
        List<Competitor> competitorsList = new ArrayList<>(competitors.values());
        competitorsList.sort((c1, c2) -> {
            try {
                return Integer.compare(getCompetitorScore(c1), getCompetitorScore(c2));
            } catch (Exception_NoResult | Exception_UnknownCompetitor e) {
                return 0;
            }
        });
        return competitorsList;
    }

    public List<Team> teamRanking() {
        Set<Team> teams = new HashSet<>();
        for (Competitor competitor : competitors.values()) {
            teams.add(competitor.getTeam());
        }
        List<Team> teamList = new ArrayList<>(teams);
        teamList.sort((t1, t2) -> {
            try {
                return Double.compare(getTeamScore(t1), getTeamScore(t2));
            } catch (Exception_NoResult | Exception_UnknownCompetitor e) {
                return 0;
            }
        });
        return teamList;
    }

    public List<Country> countryRanking() {
        Set<Country> countries = new HashSet<>();
        for (Competitor competitor : competitors.values()) {
            countries.add(competitor.getNationality());
        }
        List<Country> countryList = new ArrayList<>(countries);
        countryList.sort((c1, c2) -> {
            try {
                return Double.compare(getCountryScore(c1), getCountryScore(c2));
            } catch (Exception_NoResult | Exception_UnknownCompetitor e) {
                return 0;
            }
        });
        return countryList;
    }

    public Set<Competitor> deserterSet() {
        Set<Competitor> deserters = new HashSet<>();
        for (Competitor competitor : competitors.values()) {
            boolean finished = true;
            for (Stage stage : stages) {
                try {
                    stage.getScore(competitor);
                } catch (Exception_NoResult e) {
                    finished = false;
                    break;
                }
            }
            if (!finished) {
                deserters.add(competitor);
            }
        }
        return deserters;
    }

}

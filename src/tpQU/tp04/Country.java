package tpQU.tp04;

public enum Country {
    FRANCE("FR"),
    GERMANY("GE"),
    RUSSIA("RS"),
    SWEDEN("SE"),
    AUSTRIA("AT"),
    ITALY("IT");

    private final String NAME;

    private Country(String name){
        this.NAME = name;
    }

    public String getNAME() {
        return NAME;
    }
}

package tpQU.tp04;

public class Exercice2 {

    public static int[] tab = {17, 12, 15, 38, 29, 157, 89, -22, 0, 5};

    public static int division(int index, int divisor) {
        return tab[index]/divisor;
    }

    public static void statement() {
        try{
            int x = LocalKeyboard.readInt("Write the index of the integer to divide: ");
            int y = LocalKeyboard.readInt("Write the divisor: ");
            System.out.println("The result is: " + division(x,y));
        }
        catch(KeyboardException e){
            throw new KeyboardException();
        }
    }
}
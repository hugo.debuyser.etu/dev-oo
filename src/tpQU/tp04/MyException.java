package tpQU.tp04;

public class MyException extends Exception {
    public MyException() {}
    public MyException(String msg) {super(msg);}

    //question 1
    public void example1() throws Exception {//Il faut ajouter un throws Exception
        doIt();
    }
    public int doIt() throws Exception {
        throw new Exception();
    }

    //question 2
    public void example2() throws Exception{//Ajouter throw Exception
        throw new MyException();
    }

    //question 3
    public void doItAgain() throws MyException {
        throw new MyException();
    }

    public void example3() throws MyException {
        try {
            doItAgain();
        } catch (Exception e) {
            processing();
        }
    }

    public void processing() throws MyException{
        throw new MyException();
    }
    
    //question 4
    public int example4() {
        int data = 0;
        try {
            data = getData();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return data;
    }

    public int getData() throws NullPointerException {
        throw new NullPointerException();
    }

    //question 5
    public void example5() {
        try{
            doItFinally();
        }
        catch(RuntimeException e){
            e.printStackTrace();
        }
    }

    public int doItFinally() {
        throw new RuntimeException();
    }

    
    //question 6
    public static void main(String[] args) throws MyException{
        int k = 0;
        try {
            k = 1/Integer.parseInt(args[0]);
        }
        catch(ArrayIndexOutOfBoundsException e) {System.err.println("Index " + e);}
        catch(ArithmeticException e) {System.err.println("Arithmetic " + "");}
        catch(RuntimeException e) {System.err.println("Runtime " + e);}
    }

}
package tpQU.tp04;

public class Thing {
    private String name;
    private int quantity;
    private Container container;

    public Thing(String name, int quantity, Container container) throws ExtendedException{
        this.name = name;
        this.container = container;
        setQuantity(quantity);
    }

    private void setQuantity(int quantity) throws ExtendedException {
        if (quantity <= container.getCapacity()) {
            this.quantity = quantity;
        } else {
            throw new ExtendedException("Capacité dépassée !");
        }
    }

    public boolean transferToLargerContainer(){
        if(container.ordinal()<Container.values().length-1){
            container = Container.values()[container.ordinal()+1];
            return true;
        }
        return false;
    }

    public boolean add(int quantity){
        try{
            setQuantity(this.quantity + quantity);
            return true;
        }
        catch(ExtendedException e){
            if(!((this.quantity + quantity)>Container.CRATE.getCapacity())){
                transferToLargerContainer();
                return add(quantity);
            }else{
                System.err.println("Addition impossible");
                return false;
            }
        }
    }

    @Override
    public String toString() {
            return "Thing [name=" + this.name + ", quantity=" + this.quantity + ", container=" + this.container + "]";
    }

    public static void main(String[] args) throws ExtendedException {
        Thing ch1 = new Thing("uneThing", 3, Container.BOX);
        System.out.println(ch1);
        ch1.add(20);
        System.out.println(ch1 + "\n");
        Thing ch2 = new Thing("maThing", 10, Container.BOX);
        System.out.println(ch2);
        ch2.add(60);
        System.out.println(ch2 + "\n");
    }  

}

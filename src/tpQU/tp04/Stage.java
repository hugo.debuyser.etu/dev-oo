package tpQU.tp04;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Stage {
    private static int counter = 0;
    private final int ID;
    protected Map<Competitor, Integer> results;

    public Stage() {
        this.ID = counter++;
        this.results = new HashMap<>();
    }

    public void clear(){
        results.clear();
    }
    
    public static void resetCpt() {
        counter = 0;
    }

    public int getNbRecord(){
        return results.size();
    }

    public void record(Competitor aCompetitor, int value) throws Exception_InvalidRecord{
        if(results.containsKey(aCompetitor)){
            throw new Exception_InvalidRecord("Record already exists for this competitor.");
        }
        results.put(aCompetitor, value);
    }

    public int getScore(Competitor aCompetitor) throws Exception_NoResult{
        if(!results.containsKey(aCompetitor)){
            throw new Exception_NoResult("No results found for this competitor");
        }
        return results.get(aCompetitor);
    }

    public int getScore(List<Competitor> competitors) throws Exception_NoResult {
        int totalScore = 0;
        for (Competitor competitor : competitors) {
            totalScore += getScore(competitor);
        }
        return totalScore;
    }

    public int getID() {
        return ID;
    }

    public Map<Competitor, Integer> getresults() {
        return results;
    }

    public abstract String toString();

    public abstract String getType();

    public Integer getLength() {
        return null;
    }

    public Integer getNbShots() {
        return null;
    }
}

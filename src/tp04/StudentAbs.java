//Hugo DEBUYSER
package tp04;

public class StudentAbs {
    //attributes
    private Student etu;
    private int nbAbsence;

    //constructor
    private StudentAbs(Student etu, int nbAbsence) {
        this.etu = etu;
        this.nbAbsence = nbAbsence;
    }

    public StudentAbs(String forename, String name, int nbAbsence){
        this(new Student(forename, name), nbAbsence);
    }

    public StudentAbs(String forename, String name){
        this(forename, name, 0);
    }

    //methods
    public Student getEtu() {
        return etu;
    }

    public int getNbAbsence() {
        return nbAbsence;
    }

    public void setNbAbsence(int nbAbsence) {
        this.nbAbsence = nbAbsence;
    }

    public void addGrades(double aGrade) {
        this.etu.addGrade(aGrade);
    }

    public boolean warning(int thresholdAbs, double thresholdAvg) {
        return nbAbsence >= thresholdAbs || etu.getAverage() <= thresholdAvg;
    }

    public boolean validation(int thresholdAbs, double thresholdAvg) {
        return nbAbsence < thresholdAbs && etu.getAverage() > thresholdAvg;
    }

    public String toString() {
        return this.etu.toString() + ", nbAbs = " + this.nbAbsence;
    }

    public boolean equals(Object obj) {
        if(this == obj) return true;
        if(obj == null) return false;
        if(this.getClass() != obj.getClass()) return false;
        StudentAbs other = (StudentAbs) obj;
        if(this.etu == null) {
            if(other.etu != null) return false;
        }else if(!this.etu.equals(other.etu)) return false;
        if(this.nbAbsence != other.nbAbsence) return false;
        return true;

    }
}

//Hugo DEBUYSER

package tp04;

public class Person {
    //attributes
    private int ID;
    private String forename;
    private String name;

    private static int nextID = 1;

    //constructor
    public Person(String forename, String name){
        this.ID = nextID++;
        this.forename = forename;
        this.name = name;
    }

    public void setForename(String forename){
        this.forename = forename;
    }

    public void setName(String name){
        this.name = name;
    }

    //methods
    public String getForename() {
        return forename;
    }

    public int getID() {
        return ID;
    }

    public String getName() {
        return name;
    }

    public String toString() {
        return this.ID + ":" + this.forename + this.name;
    }

    public boolean equals(Person autre){
        if(this == null || autre == null){
            return false;
        }
        return (this.ID == autre.ID);
    }
    
}

package tp04;

/**
 * Classe représentant un groupe d'étudiants avec des notes.
 * @author Votre Nom
 */
public class YearGroup {
    /**
     * Tableau contenant les objets StudentAbs représentant les étudiants du groupe.
     */
    private StudentAbs[] yg;

    /**
     * Constructeur initialisant le groupe d'étudiants avec un tableau d'objets StudentAbs.
     * @param yg Tableau d'objets StudentAbs représentant les étudiants du groupe.
     */
    public YearGroup(StudentAbs[] yg) {
        this.yg = yg;
    } 

    /**
     * Méthode pour définir le tableau d'étudiants du groupe.
     * @param yg Nouveau tableau d'objets StudentAbs représentant les étudiants du groupe.
     */
    public void setYG(StudentAbs[] yg) {
        this.yg = yg;
    }

    /**
     * Méthode pour obtenir le tableau d'étudiants du groupe.
     * @return Tableau d'objets StudentAbs représentant les étudiants du groupe.
     */
    public StudentAbs[] getYG() {
        return this.yg;
    }

    /**
     * Ajoute des notes à chaque étudiant du groupe.
     * @param aTest Tableau de doubles représentant les notes à ajouter.
     */
    public void addGrades(double[] aTest) {
        for(int idx = 0; idx<aTest.length; idx++) {
            this.yg[idx].addGrades(aTest[idx]);
        }
    }

    /**
     * Valide chaque étudiant du groupe en fonction de seuils spécifiques.
     * @param thresholdAbs Seuil absolu pour la validation.
     * @param thresholdAvg Seuil moyen pour la validation.
     */
    public void validation(int thresholdAbs, int thresholdAvg) {
        for(int idx =0; idx<this.yg.length; idx++) {
            if(this.yg[idx].validation(thresholdAbs, thresholdAvg)) {
                System.out.println(this.yg[idx]);
            }
        }
    }
}

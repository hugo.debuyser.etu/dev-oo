package tp04;

import tpOO.tp04.PendingCase;

import java.util.Arrays;

/**
 * Classe représentant une file d'attente de cas pendants.
 * @author Hugo D
 */
public class PendingCaseQueue {
    /**
     * Capacité maximale de la file d'attente.
     */
    public static int CAPACITY = 10;
    /**
     * Tableau contenant les cas pendants.
     */
    private PendingCase[] queue;
    /**
     * Index du dernier élément ajouté dans la file d'attente.
     */
    private int idx;

    /**
     * Constructeur initialisant la file d'attente avec une capacité fixe.
     */
    public PendingCaseQueue () {
        this.idx = 0;
        this.queue = new PendingCase[CAPACITY];
    }

    /**
     * Vide la file d'attente en réinitialisant son contenu.
     */
    public void clear() {
        this.queue = new PendingCase[CAPACITY];
        idx = 0;
    }

    /**
     * Vérifie si la file d'attente est vide.
     * @return true si la file d'attente est vide, false sinon.
     */
    public boolean isEmpty() {
        return this.idx == 0;
    }

    /**
     * Vérifie si la file d'attente est pleine.
     * @return true si la file d'attente est pleine, false sinon.
     */
    public boolean isFull() {
        return this.idx == (this.queue.length);
    }

    /**
     * Renvoie la taille actuelle de la file d'attente.
     * @return La taille actuelle de la file d'attente.
     */
    public int size() {
        return this.idx+1;
    }

    /**
     * Ajoute un cas pendants à la file d'attente.
     * @param other Le cas pendants à ajouter.
     */
    public void addOne(PendingCase other) {
        if (!isFull()) {
            if (this.idx == this.queue.length) {
                this.queue = Arrays.copyOf(this.queue, this.queue.length * 2);
            }
            this.queue[this.idx] = other;
            this.idx++;
        }
    }

    /**
     * Supprime et renvoie le premier cas pendants de la file d'attente.
     * @return Le premier cas pendants de la file d'attente, null si la file est vide.
     */
    public PendingCase removeOne() {
        if (!isEmpty()) {
            this.queue = Arrays.copyOfRange(this.queue, 1, CAPACITY);
            this.idx = this.idx-1;
            return this.queue[0];
        }
        return null;
    }

    /**
     * Calcule et renvoie le montant total des cas pendants dans la file d'attente.
     * @return Le montant total des cas pendants.
     */
    public double getTotalAmount() {
        double total =0;
        for (int i = 0; i < idx; i++) {
            total += this.queue[i].getAmount();
        }
        return total;
    }

    /**
     * Insère un cas pendants à une position spécifique dans la file d'attente.
     * @param another Le cas pendants à insérer.
     * @param position La position à laquelle insérer le cas pendants.
     */
    public void cheating(PendingCase another, int position) {
        if (!isFull()) {
            for (int i = idx; i > position; i--) {
                queue[i] = queue[i - 1];
            }
            queue[position] = another;
            idx++;
        }
    }

    /**
     * Renvoie une représentation textuelle de la file d'attente.
     * @return La représentation textuelle de la file d'attente.
     */
    public String toString() {
        return Arrays.toString(this.queue);
    }
}

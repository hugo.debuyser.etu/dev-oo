package tp04;

public class UseStudent {
    public static void main(String[] args) {
		Student alice = new Student("Alice", "A");
		Student bruno = new Student("Bruno", "B");
        alice.addGrade(14);
        alice.addGrade(18);
        alice.addGrade(11);
        bruno.addGrade(0);
        bruno.addGrade(13);
        bruno.addGrade(12);
        System.out.println(alice.toString());
        System.out.println(bruno.toString());
        System.out.println(alice.getAverage());
        System.out.println(bruno.getAverage());
        Student autre = alice;
        System.out.println(alice.equals(autre));
        System.out.println(bruno.equals(alice));
    }
}

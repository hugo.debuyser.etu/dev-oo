package tp04;

import java.util.Arrays;
import java.time.LocalDate;

/**
 * Classe représentant une version immuable de la classe Thing.
 * @author Hugo D
 */
public class ImmutableThing {

    /**
     * Numéro unique de l'objet.
     */
    private int number;
    /**
     * Référence de l'objet.
     */
    private String ref;
    /**
     * Date de création ou d'utilisation de l'objet.
     */
    private LocalDate date;
    /**
     * Tableau de nombres associés à l'objet.
     */
    private int[] numbers;
    /**
     * Point d'origine de l'objet.
     */
    private Point origin;

    /**
     * Constructeur de la classe ImmutableThing initialisant tous ses attributs.
     * @param number Numéro unique de l'objet.
     * @param ref Référence de l'objet.
     * @param date Date de création ou d'utilisation de l'objet.
     * @param origine Point d'origine de l'objet.
     * @param nombres Tableau de nombres associés à l'objet.
     */
    public ImmutableThing(int number, String ref, LocalDate date, Point origine, int[] nombres) {
        this.number = number;
        this.ref = "" + ref; // Conversion en String pour garantir l'immutabilité
        this.date = date;
        this.origin = new Point(origine.getX(), origine.getY()); // Création d'une copie du point
        this.numbers = Arrays.copyOf(nombres, nombres.length); // Création d'une copie du tableau
    }

    /**
     * Getteur pour le numéro de l'objet.
     * @return Le numéro de l'objet.
     */
    public int getNumber() {
        return this.number;
    }

    /**
     * Getteur pour la référence de l'objet.
     * @return La référence de l'objet.
     */
    public String getRef() {
        return this.ref;
    }

    /**
     * Getteur pour la date de l'objet.
     * @return La date de l'objet.
     */
    public LocalDate getDate() {
        return this.date;
    }

    /**
     * Getteur pour le tableau de nombres associés à l'objet.
     * @return Le tableau de nombres.
     */
    public int[] getNumbers() {
        return Arrays.copyOf(this.numbers, this.numbers.length); // Retourne une copie du tableau
    }

    /**
     * Getteur pour le point d'origine de l'objet.
     * @return Le point d'origine de l'objet.
     */
    public Point getPoint() {
        return new Point(this.origin.getX(), this.origin.getY()); // Retourne une copie du point
    }

    /**
     * Renvoie une représentation textuelle de l'objet ImmutableThing.
     * @return La représentation textuelle de l'objet.
     */
    @Override
    public String toString() {
        return "ImmutableThing{" +
                "number=" + number +
                ", ref='" + ref + '\'' +
                ", date=" + date +
                ", origin=" + origin +
                ", numbers=" + Arrays.toString(numbers) +
                '}';
    }

    /**
     * Crée une nouvelle instance de ImmutableThing à partir d'une instance de Thing.
     * @param aThing Instance de Thing à sécuriser.
     * @return Nouvelle instance de ImmutableThing.
     */
    public ImmutableThing secure(Thing aThing) {
        return new ImmutableThing(aThing.getNumber(), aThing.getRef(), aThing.getDate(), aThing.getOrigin(), aThing.getNumbers());
    }
}

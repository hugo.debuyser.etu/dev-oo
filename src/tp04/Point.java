package tp04;

/**
 * Classe représentant un point dans un espace 2D avec des coordonnées x et y.
 * @author Hugo D
 */
public class Point {

    /**
     * Coordonnée x du point.
     */
    private double x;
    /**
     * Coordonnée y du point.
     */
    private double y;

    /**
     * Constructeur de la classe Point initialisant les coordonnées x et y.
     * @param x Coordonnée x du point.
     * @param y Coordonnée y du point.
     */
    public Point(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Ajoute des valeurs aux coordonnées x et y du point.
     * @param x Valeur à ajouter à la coordonnée x.
     * @param y Valeur à ajouter à la coordonnée y.
     */
    public void add(double x, double y) {
        this.x = this.x + x;
        this.y = this.y + y;
    }

    /**
     * Getteur pour la coordonnée x du point.
     * @return La coordonnée x du point.
     */
    public double getX() {
        return this.x;
    }

    /**
     * Getteur pour la coordonnée y du point.
     * @return La coordonnée y du point.
     */
    public double getY() {
        return this.y;
    }

    /**
     * Renvoie une représentation textuelle du point sous la forme "(x,y)".
     * @return La représentation textuelle du point.
     */
    @Override
    public String toString() {
        return "(" + this.x + "," + this.y + ")";
    }
}

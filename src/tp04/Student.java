//Hugo DEBUYSER
package tp04;

public class Student {
    //attributes
    private Person pers;
    private double[] grades;

    //constructor
    private Student(Person pers, double[] grades){
        this.pers = pers;
        this.grades = grades;
    }
    public Student(String forename, String name, double[] grades){
        this(new Person(forename, name), grades);
    }
    public Student(String forename, String name){
        this(forename, name, new double[0]);
    }

    //methods
    public String getPersName() {
        return this.pers.getName();
    }

    public String getPersForename(){
        return this.pers.getForename();
    }

    public int getPersID(){
        return this.pers.getID();
    }

    public void setPersName(String name) {
        this.pers.setName(name);
    }

    public void setPersForename(String forename){
        this.pers.setForename(forename);
    }

    private String doubleArrayToString(double[]liste){
        String retour = "";
        for(int i = 0; i<liste.length;i++){
            retour = retour  + liste[i];
            if(i < liste.length-1){
                retour = retour + ",";
            }
        }
        return retour;
    }

    public String toString() {
        return "Student[" + this.getPersID() + "]: " + this.getPersForename() + this.getPersName() + " = [" + doubleArrayToString(this.grades) + "]";
    }

    public boolean equals(Student other){
        if(this.grades.length != other.grades.length || other == null || this == null){
            return false;
        }
        if(other == this){
            return true;
        }
        for(int i = 0; i<other.grades.length;i++){
            if(other.grades[i] != this.grades[i]){
                return false;
            }
        }
        return true;
    }

    public double getAverage(){
        if(this.grades.length == 0){
            return 0;
        }
        double total = 0;
        for(int i = 0; i<this.grades.length;i++){
            total = total + this.grades[i];
        }
        return total/this.grades.length;
    }

    public void addGrade(double grade){
        this.grades = new double[this.grades.length+1];
        this.grades[this.grades.length-1] = grade; 
    }

}

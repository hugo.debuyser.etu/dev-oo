package tp04;

import java.time.LocalDate;

/**
 * Classe représentant un objet "Thing" avec des attributs variés.
 * @author Votre Hugo D
 */
public class Thing {

    /**
     * Numéro unique de l'objet.
     */
    private int number;
    /**
     * Référence de l'objet.
     */
    private String ref;
    /**
     * Date de création ou d'utilisation de l'objet.
     */
    private LocalDate date;
    /**
     * Tableau de nombres associés à l'objet.
     */
    private int[] numbers;
    /**
     * Point d'origine de l'objet, peut représenter une position géographique ou un point de référence.
     */
    private Point origin;

    /**
     * Constructeur de la classe Thing initialisant tous ses attributs.
     * @param number Numéro unique de l'objet.
     * @param ref Référence de l'objet.
     * @param date Date de création ou d'utilisation de l'objet.
     * @param origin Point d'origine de l'objet.
     * @param nombres Tableau de nombres associés à l'objet.
     */
    public Thing(int number, String ref, LocalDate date, Point origin, int[] nombres) {
        this.number = number;
        this.ref = ref;
        this.date = date;
        this.origin = origin;
        this.numbers = nombres;
    }

    /**
     * Getteur pour le numéro de l'objet.
     * @return Le numéro de l'objet.
     */
    public int getNumber() {
        return this.number;
    }

    /**
     * Getteur pour le tableau de nombres associés à l'objet.
     * @return Le tableau de nombres.
     */
    public int[] getNumbers() {
        return this.numbers;
    }

    /**
     * Getteur pour la référence de l'objet.
     * @return La référence de l'objet.
     */
    public String getRef() {
        return this.ref;
    }

    /**
     * Getteur pour le point d'origine de l'objet.
     * @return Le point d'origine de l'objet.
     */
    public Point getOrigin() {
        return this.origin;
    }

    /**
     * Getteur pour la date de l'objet.
     * @return La date de l'objet.
     */
    public LocalDate getDate() {
        return this.date;
    }
}

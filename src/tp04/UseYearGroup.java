package tp04;

public class UseYearGroup {
    public static void main(String[] args) {
        
        StudentAbs A = new StudentAbs("a", "A",0);
        StudentAbs B = new StudentAbs("b", "B",1);
        StudentAbs C = new StudentAbs("b", "C", 10);
        StudentAbs D = new StudentAbs("c", "D",5);

        A.addGrades(15.0);
        C.addGrades(15.0);
        B.addGrades(18.0);
        D.addGrades(2.0);

        YearGroup yg = new YearGroup(new StudentAbs[] {A,B,C,D});
        yg.validation(2, 10);

    }
}

package tp05;

public class UseLibrary {
    public static void main(String[] args) {
        Book book1 = new Book("H2G2", "The Hitchhikerâ€™s Guide to the Galaxy", "D. Adams", 1979);
        Book book2 = new Book("FLTL", "Flatland", "E.Abbott Abbott", 1884);
        Book book3 = new Book("REU", "The Restaurant at the End of the Universe", "D. Adams", 1980);
        Book comic1= new ComicBook("44", "A", "effe", 2024, "A");

        Library commodeALivre = new Library();
        commodeALivre.addBook(book1);
        commodeALivre.addBook(book2);
        commodeALivre.addBook(book3);
        commodeALivre.addBook(comic1);

        System.out.println("A specific book:");
        System.out.println(commodeALivre.getBook("H2G2").toString());

        System.out.println("Display of the library");
        System.out.println(commodeALivre.toString());

        System.out.println();

        System.out.println("Borrowings list");
        System.out.println("Initially -->");
        commodeALivre.borrowings();        

        System.out.print(commodeALivre.borrow("H2G2", 42) + " --> ");
        System.out.println(commodeALivre.borrowings());

        System.out.print(commodeALivre.borrow("H2G2", 404) + " --> ");
        System.out.println(commodeALivre.borrowings());

        System.out.print(commodeALivre.borrow("REU", 404) + "--> ");
        System.out.println(commodeALivre.borrowings());

        System.out.print(commodeALivre.borrow("FLTL", 42) + "true --> ");
        System.out.println(commodeALivre.borrowings());

        System.out.print(commodeALivre.borrow("44", 42) + "true --> ");
        System.out.println(commodeALivre.borrowings());

        System.out.println("Dates de retour:");
        System.out.println(book1.getGiveBackDate());
        System.out.println(comic1.getGiveBackDate());

    }
}

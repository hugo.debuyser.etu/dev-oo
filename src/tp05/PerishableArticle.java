package tp05;

import java.time.LocalDate;

/**
 * Classe représentant un article périssable dans un magasin.
 * Étend la classe Article.
 */
public class PerishableArticle extends Article {
    // Date limite de péremption
    private LocalDate deadLine;

    // Constructeur avec toutes les informations
    public PerishableArticle(String idRef, String label, double purchasePrice, double salePrice, LocalDate deadline) {
        super(idRef, label, purchasePrice, salePrice);
        this.deadLine = deadline;
    }

    // Constructeur sans spécifier le prix de vente
    public PerishableArticle(String idRef, String label, double purchasePrice, LocalDate deadline) {
        super(idRef, label, purchasePrice);
        this.deadLine = deadline;
    }

    // Constructeur avec une date limite par défaut (10 jours à partir de maintenant)
    public PerishableArticle(String idRef, String label, double purchasePrice, double salePrice) {
        super(idRef, label, purchasePrice, salePrice);
        this.deadLine = LocalDate.now().plusDays(10);
    }

    // Constructeur sans spécifier le prix de vente et avec une date limite par défaut (10 jours à partir de maintenant)
    public PerishableArticle(String idRef, String label, double purchasePrice) {
        super(idRef, label, purchasePrice);
        this.deadLine = LocalDate.now().plusDays(10);
    }

    // Accesseur pour la date limite de péremption
    public LocalDate getDeadLine() {
        return this.deadLine;
    }

    // Surcharge de la méthode isPerishable de la classe parente
    @Override
    public boolean isPerishable() {
        return true;
    }

    // Surcharge de la méthode toString pour afficher les informations de l'article périssable
    @Override
    public String toString() {
        return "PerishableArticle [" + super.getIdRef() + ":" + super.getLabel() + "=" +
                super.getPURCHASE_PRICE() + "€/" + super.getSalePrice() + "€-->" + this.deadLine + "]";
    }
}

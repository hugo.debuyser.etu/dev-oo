package tp05;

/**
 * Classe représentant un article d'un magasin.
 */
public class Article {
    /**
     * ID de l'article
     */
    private String idRef;
    /**
     * Label associé à l'article
     */
    private String label;
    /**
     * Prix d'achat de l'article
     */
    private final double PURCHASE_PRICE;
    /**
     * Prix de vente de l'article
     */
    private double salePrice;

    /**
     * Constructeur de la classe Article
     * @param idRef ID de l'article
     * @param label Label associé à l'article
     * @param purchasePrice Prix d'achat de l'article
     * @param salePrice Prix de vente de l'article
     */
    public Article(String idRef, String label, double purchasePrice, double salePrice){
        this.idRef = idRef;
        this.label = label;
        this.PURCHASE_PRICE = purchasePrice;
        this.salePrice = salePrice;
    }

    /**
     * Constructeur alternatif pour un article sans prix de vente spécifié.
     * Le prix de vente est automatiquement fixé à 20% plus élevé que le prix d'achat.
     * @param idRef ID de l'article
     * @param label Label associé à l'article
     * @param purchasePrice Prix d'achat de l'article
     */
    public Article(String idRef, String label, double purchasePrice) {
        this(idRef, label, purchasePrice, purchasePrice*1.2);
    }

    /**
     * Méthode permettant d'obtenir le prix d'achat de l'article.
     * @return Le prix d'achat de l'article
     */
    public double getPURCHASE_PRICE() {
        return this.PURCHASE_PRICE;
    }

    /**
     * Méthode permettant d'obtenir le prix de vente de l'article.
     * @return Le prix de vente de l'article
     */
    public double getSalePrice() {
        return this.salePrice;
    }

    /**
     * Méthode permettant d'obtenir l'ID de référence de l'article.
     * @return L'ID de référence de l'article
     */
    public String getIdRef(){
        return this.idRef;
    }

    /**
     * Méthode permettant d'obtenir le label de l'article.
     * @return Le label de l'article
     */
    public String getLabel(){
        return this.label;
    }

    /**
     * Méthode permettant de définir le prix de vente de l'article.
     * @param salePrice Le prix de vente de l'article à définir
     */
    public void setSalePrice(double salePrice) {
        this.salePrice = salePrice;
    }

    /**
     * Méthode permettant de calculer la marge bénéficiaire de l'article.
     * @return La marge bénéficiaire de l'article
     */
    public double getMargin() {
        return this.salePrice - this.PURCHASE_PRICE;
    }

    /**
     * Méthode indiquant si l'article est périssable.
     * Ici, tous les articles sont considérés comme non périssables.
     * @return True si l'article est périssable, sinon False
     */
    public boolean isPerishable() {
        return false;
    }

    /**
     * Méthode de représentation textuelle de l'article.
     * @return Une chaîne de caractères représentant l'article avec son ID, label, prix d'achat et prix de vente
     */
    @Override
    public String toString() {
        return "Article [" + this.idRef + ":" + this.label + "=" + this.PURCHASE_PRICE + "€/" + this.salePrice + "€]";
    }
}
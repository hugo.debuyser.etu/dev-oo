package tp05;

import java.util.ArrayList;

/**
 * Classe représentant une bibliothèque de livres.
 * @author Hugo D
 */
public class Library {

    /**
     * Attribut pour la bibliothèque de livre qui est une liste de livre
     */
    private ArrayList<Book> catalog;

    /**
     * Initialise le catalogue de livres avec une liste vide.
     */
    public Library() {
        this(new ArrayList<Book>());
    }

    /**
     * Constructeur avec une liste de livres préexistante.
     * @param catalog La liste de livres à initialiser dans la bibliothèque.
     */
    public Library(ArrayList<Book> catalog) {
        this.catalog = catalog;
    }

    /**
     * Recherche un livre dans la bibliothèque par son code.
     * @param code Le code du livre à rechercher.
     * @return Le livre correspondant au code, ou null si le livre n'est pas trouvé.
     */
    public Book getBook(String code){
        for(Book book : catalog) {
            if(book.getCode().equals(code)) {
                return book;
            }
        }
        return null;
    }

    /**
     * Ajoute un livre à la bibliothèque s'il n'est pas déjà présent.
     * @param b Le livre à ajouter.
     * @return true si le livre a été ajouté, false sinon.
     */
    public boolean addBook(Book b){
        if(!this.catalog.contains(b)){
            this.catalog.add(b);
            return true;
        }
        return false;
    }

    /**
     * Supprime un livre de la bibliothèque par son code.
     * @param aCode Le code du livre à supprimer.
     * @return true si le livre a été supprimé, false sinon.
     */
    public boolean removeBook(String aCode){
        for(Book book : catalog) {
            if(book.getCode().equals(aCode)) {
                catalog.remove(book);
                return true;
            }
        }
        return false;
    }

    /**
     * Supprime un livre spécifique de la bibliothèque.
     * @param b Le livre à supprimer.
     * @return true si le livre a été supprimé, false sinon.
     */
    public boolean removeBook(Book b){
        return this.catalog.remove(b);
    }
     
    /**
     * Liste les emprunts en cours dans la bibliothèque.
     * @return Une chaîne de caractères contenant les codes des livres empruntés et les identifiants des emprunteurs.
     */
    public String borrowings(){
        StringBuilder renvoie = new StringBuilder();
        for(Book book : catalog) {
            if(!book.isAvailable()){
                renvoie.append("(").append(book.getCode()).append(")").append("--").append(book.getBorrower());
            }
        }
        return renvoie.toString();
    }

    /**
     * Emprunte un livre par son code.
     * @param code Le code du livre à emprunter.
     * @param borrower L'identifiant de la personne emprunteuse.
     * @return true si le livre a été emprunté, false sinon.
     */
    public boolean borrow(String code, int borrower){
        Book book = getBook(code);
        return book != null && book.borrow(borrower);
    }

    /**
     * Rend un livre par son code.
     * @param code Le code du livre à rendre.
     * @return true si le livre a été rendu, false sinon.
     */
    public boolean giveBack(String code){
        Book book = getBook(code);
        return book != null && book.giveBack();
    }

    /**
     * Obtient le nombre de livres empruntés dans la bibliothèque.
     * @return Le nombre de livres empruntés.
     */
    public int borrowedBookNumber() {
        int nb = 0;
        for(Book book : catalog) {
            if(!book.isAvailable()) {
                nb++;
            }
        }
        return nb;
    }

    /**
     * Obtient la taille du stock de la bibliothèque.
     * @return La taille du stock.
     */
    public int stockSize() {
        return this.catalog.size();
    }

    /**
     * Retourne une représentation sous forme de chaîne de caractères de la bibliothèque.
     * @return La liste des livres dans la bibliothèque.
     */
    public String toString(){
        return this.catalog.toString();
    }
}
package tp05;

import java.time.LocalDate;

/**
 * Classe représentant une bande dessinée dans une bibliothèque héritant de la classe Book.
 * @author Hugo D
 */
public class ComicBook extends Book {
    private String illustrator;
    private int durationMax = 5;

    /**
     * Constructeur de la classe ComicBook.
     * @param code Code unique de la bande dessinée.
     * @param title Titre de la bande dessinée.
     * @param author Auteur de la bande dessinée.
     * @param publicationYear Année de publication de la bande dessinée.
     * @param illustrator Illustrateur de la bande dessinée.
     */
    public ComicBook(String code, String title, String author, int publicationYear, String illustrator) {
        super(code, title, author, publicationYear);
        this.illustrator = illustrator;
    }

    /**
     * Obtient la durée maximale qui est de 5 jours si la bande dessinée est parue il y a 2 ans ou moins ou de 15 jours si plus.
     * @return La durée maximale d'emprunt.
     */
    @Override
    public int getDurationMax() {
        int currentYear = LocalDate.now().getYear();
        if (currentYear - this.getPublicationYear() <= 2) {
            return this.durationMax;
        } else {
            return super.getDurationMax();
        }
    }

    /**
     * Obtient la date de retour prévue pour la bande dessinée.
     * @return La date de retour prévue.
     */
    @Override
    public LocalDate getGiveBackDate() {
        return this.getBorrowingDate().plusDays(getDurationMax());
    }

    /**
     * Redéfinition de la méthode toString pour afficher les informations spécifiques aux bandes dessinées.
     * @return Les informations de la bande dessinée.
     */
    @Override
    public String toString() {
        return "ComicBook[" + this.getCode() + ":" + this.getTitle() + "->" + this.getAuthor() + "," + this.getPublicationYear() + "," + this.illustrator + "]";
    }
}
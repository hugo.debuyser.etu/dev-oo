package tp05;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Classe représentant un magasin qui vend des articles.
 */
public class Shop {
    // Liste des articles en vente dans le magasin
    private List<Article> catalog = new ArrayList<>();

    // Méthode de représentation textuelle du magasin
    public String toString() {
        StringBuilder result = new StringBuilder();
        for (Article article : catalog) {
            result.append(article).append(System.getProperty("line.separator"));
        }
        return result.toString();
    }

    // Méthode pour ajouter un article au catalogue
    public void addArticle(Article article) {
        this.catalog.add(article);
    }

    // Méthode pour ajouter plusieurs articles au catalogue
    public void addArticle(List<Article> articles) {
        catalog.addAll(articles);
    }

    // Méthode pour obtenir la liste des articles périssables dans le catalogue
    public List<PerishableArticle> getPerishables() {
        List<PerishableArticle> perishableList = new ArrayList<>();
        for (Article article : this.catalog) {
            if (article instanceof PerishableArticle) {
                perishableList.add((PerishableArticle) article);
            }
        }
        return perishableList;
    }

    // Méthode pour obtenir le nombre total d'articles dans le catalogue
    public int getNbArticle() {
        return this.catalog.size();
    }

    // Méthode pour obtenir le nombre d'articles périssables dans le catalogue
    public int getNbPerishableArticle() {
        return getPerishables().size();
    }

    // Méthode pour appliquer une réduction sur les articles périssables avant une certaine date
    public void discountPerishable(LocalDate threshold, double rate) {
        for (PerishableArticle perishable : getPerishables()) {
            if (perishable.getDeadLine().isBefore(threshold) || perishable.getDeadLine().isEqual(threshold)) {
                perishable.setSalePrice(perishable.getSalePrice() * (1 - rate));
            }
        }
    }

    // Méthode pour appliquer une réduction sur les articles non périssables
    public void discountNotPerishable(double rate) {
        for (Article article : catalog) {
            if (!(article instanceof PerishableArticle)) {
                double currentPrice = article.getSalePrice();
                article.setSalePrice(currentPrice * (1 - rate));
            }
        }
    }

    // Méthode pour trouver l'article le plus rentable dans le catalogue
    Article mostProfitable() {
        Article article = catalog.get(0);
        for (Article currentArticle : this.catalog) {
            if (currentArticle.getMargin() > article.getMargin()) {
                article = currentArticle;
            }
        }
        return article;
    }
}

package tp05;

import java.time.LocalDate;

/**
 * Classe représentant un livre dans une bibliothèque.
 * @author Hugo D
 */
public class Book {
    /**
     * Code unique du livre.
     */
    private String code;
    /**
     * Titre du livre.
     */
    private String title;
    /**
     * Auteur du livre.
     */
    private String author;
    /**
     * Année de publication du livre.
     */
    private int publicationYear;
    /**
     * Identifiant de la personne ayant emprunté le livre.
     */
    private int borrower;

    private int durationMax = 15;

    private LocalDate borrowingDate;

    /**
     * Constructeur de la classe Book.
     * @param code Code unique du livre.
     * @param title Titre du livre.
     * @param author Auteur du livre.
     * @param publicationYear Année de publication du livre.
     * @param borrower Identifiant de la personne ayant emprunté le livre.
     */
    public Book(String code, String title, String author, int publicationYear){
        this.code = code;
        this.title = title;
        this.author = author;
        this.publicationYear = publicationYear;
        this.borrower = 0;
    }

    /**
     * Obtient le code du livre.
     * @return Le code du livre.
     */
    public String getCode(){
        return this.code;
    }

    /**
     * Obtient le titre du livre.
     * @return Le titre du livre.
     */
    public String getTitle(){
        return this.title;
    }

    /**
     * Obtient l'auteur du livre.
     * @return L'auteur du livre.
     */
    public String getAuthor(){
        return this.author;
    }

    /**
     * Obtient l'année de publication du livre.
     * @return L'année de publication du livre.
     */
    public int getPublicationYear(){
        return this.publicationYear;
    }

    /**
     * Obtient l'identifiant de la personne ayant emprunté le livre.
     * @return L'identifiant de la personne ayant emprunté le livre.
     */
    public int getBorrower(){
        return this.borrower;
    }

    /**
     * Obtient la date d'emprunt du livre.
     * @return Date d'emprunt du livre.
     */
    public LocalDate getBorrowingDate() {
        return this.borrowingDate;
    }

    /**
     * Définit le code du livre.
     * @param code Le nouveau code du livre.
     */
    public void setCode(String code){
        this.code = code;
    }

    /**
     * Définit le titre du livre.
     * @param title Le nouveau titre du livre.
     */
    public void setTitle(String title){
        this.title = title;
    }

    /**
     * Définit l'auteur du livre.
     * @param author Le nouveau auteur du livre.
     */
    public void setAuthor(String author){
        this.author = author;
    }

    /**
     * Définit l'année de publication du livre.
     * @param publicationYear La nouvelle année de publication du livre.
     */
    public void setPublicationYear(int publicationYear){
        this.publicationYear = publicationYear;
    }

    /**
     * Définit l'identifiant de la personne ayant emprunté le livre.
     * @param borrower Le nouvel identifiant de la personne ayant emprunté le livre.
     */
    public void setBorrower(int borrower){
        this.borrower = borrower;
    }

    /**
     * Emprunte le livre.
     * @param borrower Identifiant de la personne emprunteuse.
     * @return true si le livre a été emprunté, false sinon.
     */
    public boolean borrow(int borrower){
        if(isAvailable()){
            this.borrower = borrower;
            this.borrowingDate = LocalDate.now();
            return true;
        }
        return false;
    }

    /**
     * Emprunte à nouveau le livre (utile pour la gestion des retards).
     * @param borrower Identifiant de la personne emprunteuse.
     * @return true si le livre a été emprunté à nouveau, false sinon.
     */
    public boolean borrowAgain(int borrower) {
        return borrow(borrower);
    }

    /**
     * Rend le livre.
     * @return true si le livre a été rendu, false sinon.
     */
    public boolean giveBack(){
        if(isAvailable()){
            return false;
        }
        this.borrower = 0;
        this.borrowingDate = null;
        return true;
    }

    /**
     * Vérifie si le livre est disponible.
     * @return true si le livre est disponible, false sinon.
     */
    public boolean isAvailable(){
        return this.borrower == 0;
    }

    /**
     * Obtient la date de retour prévue pour le livre qui est de 15 jours.
     * @return La date de retour prévue.
     */
    public LocalDate getGiveBackDate() {
        return borrowingDate.plusDays(this.durationMax);
    }

    /**
     * Obtient la durée maximale d'emprunt pour le livre.
     * @return La durée maximale d'emprunt.
     */
    public int getDurationMax() {
        return this.durationMax;
    }

    /**
     * Méthode pour vérifier l'égalité entre deux objets Book.
     * @param obj L'objet à comparer avec l'objet courant.
     * @return true si les deux objets sont égaux, sinon false.
     */
    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        Book other = (Book) obj;
        if (code == null) {
            if (other.code != null) return false;
        } else if (!code.equals(other.code)) return false;
        if (title == null) {
            if (other.title != null) return false;
        } else if (!title.equals(other.title)) return false;
        if (author == null) {
            if (other.author != null) return false;
        } else if (!author.equals(other.author)) return false;
        if (publicationYear != other.publicationYear) return false;
        return true;
    }

    /**
     * Renvoie les informations du livre sous forme de chaîne de caractères.
     * @return Les informations du livre.
     */
    @Override
    public String toString() {
        return "Book [" + this.code + ":" + this.title + "->" + this.author + "," + this.publicationYear + "]";
    }
}

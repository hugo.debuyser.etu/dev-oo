package tp05;

import java.time.LocalDate;

public class UseArticle {
    public static void main(String[] args) {
        Shop shop = new Shop();

        shop.addArticle(new Article("42err404", "chaise", 20.00));
        shop.addArticle(new PerishableArticle("43err404", "milk", 20.00, LocalDate.of(2025, 5, 10)));
        shop.addArticle(new Article("44err404", "lamp", 50.00));
        shop.addArticle(new PerishableArticle("45err404", "bread", 15.00, LocalDate.now().minusDays(5)));
        shop.addArticle(new PerishableArticle("46err404", "apple", 10.00));
        System.out.println("Articles du shop:");
        System.out.println(shop);

        shop.discountPerishable(LocalDate.now(), 0.2);
        System.out.println("Articles du shop (discount):");
        System.out.println(shop);

    }
}

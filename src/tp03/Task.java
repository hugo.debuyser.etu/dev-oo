package tp03;

import java.time.LocalDate;

public class Task {
    //attributes
    private String idTask;
    private LocalDate creationDate;
    private LocalDate deadline;
    private TaskStatus state;
    private String description;
    private int duration;

    //constructor
    public Task(String description, int duration) {
        this.description = description;
        this.duration = duration;
        this.creationDate = LocalDate.now();
        this.deadline = LocalDate.now().plusDays(10);
        this.state = TaskStatus.TODO;
        generateId();
    }

    public Task(String description, LocalDate creation, LocalDate deadline, int duration) {
        this.description = description;
        this.duration = duration;
        this.creationDate = creation;
        this.deadline = deadline;
        this.state = TaskStatus.TODO;
        generateId();
    }

    //methods
    private void generateId() {
        String year = String.valueOf(creationDate.getYear()).substring(2);
        String month = String.format("%02d", creationDate.getMonthValue());
        String projectNumber = "" + (Task.nextProjectNumber++);
        this.idTask = year + month + projectNumber;
    }

    private static int nextProjectNumber = 0;

    public String getIdTask() {
        return idTask;
    }

    public LocalDate getCreationDate() {
        return creationDate;
    }

    public LocalDate getDeadline() {
        return deadline;
    }

    public TaskStatus getState() {
        return state;
    }

    public String getDescription() {
        return description;
    }

    public int getDuration() {
        return duration;
    }

    public String toString() {
        return "(" + idTask + " = " + description + ":" + state + "(" + duration + "):" + deadline + ")";
    }

    public void changeStatus() {
        switch (state) {
            case TODO:
                state = TaskStatus.ONGOING;
                break;
            case ONGOING:
                state = TaskStatus.DELAYED;
                break;
            case DELAYED:
                state = TaskStatus.FINISHED;
                break;
            default:
                break;
        }
    }
    
    public void changeStatus(TaskStatus st) {
        state = st;
    }
    
    public void changeStatus(char c) {
        switch (Character.toUpperCase(c)) {
            case 'T':
                state = TaskStatus.TODO;
                break;
            case 'O':
                state = TaskStatus.ONGOING;
                break;
            case 'D':
                state = TaskStatus.DELAYED;
                break;
            case 'F':
                state = TaskStatus.FINISHED;
                break;
            default:
                break;
        }
    }

    public boolean isLate() {
        return LocalDate.now().isAfter(deadline);
    }

    public void delay(int nbDays) {
        deadline = deadline.plusDays(nbDays);
    }    

}

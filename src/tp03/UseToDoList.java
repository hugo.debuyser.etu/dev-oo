//Hugo Debuyser

package tp03;

public class UseToDoList {
    public static void main(String[] args) {
        ToDoList aliceTasks = new ToDoList();
        ToDoList brunoTasks = new ToDoList();

        aliceTasks.addTask(new Task("a1", 3));
        aliceTasks.addTask(new Task("a2", 4));
        aliceTasks.addTask(new Task("a3", 5));

        brunoTasks.addTask(new Task("b1", 1));
        brunoTasks.addTask(new Task("b2", 5));
        brunoTasks.addTask(new Task("b3", 9));

        System.out.println("Alice's tasks:");
        System.out.println(aliceTasks);
        System.out.println("Bruno's tasks:");
        System.out.println(brunoTasks);

        for (int i = 0; i < brunoTasks.getNbTask(); i++) {
            aliceTasks.addTask(brunoTasks.getChores()[i]);
            brunoTasks.removeTask(i);
        }

        System.out.println("Bruno se fait porter pâle pour une période indéterminée, Alice hérite donc de :");
        System.out.println(aliceTasks);
        System.out.println("Bruno a fini de se faire porter pâle, Bruno hérite donc de :");
        System.out.println(brunoTasks);

        aliceTasks.durationSort();
        Task longestTask = aliceTasks.getChores()[0];
        aliceTasks.senioritySort();
        Task oldestTask = aliceTasks.getChores()[0];

        longestTask.delay(3);
        oldestTask.delay(3);

        System.out.println("Alice obtient le report de sa tâche la plus longue et de sa tâche la plus ancienne :");
        System.out.println(aliceTasks);
    }
}

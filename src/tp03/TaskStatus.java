package tp03;

public enum TaskStatus {
    TODO, ONGOING, DELAYED, FINISHED;
}

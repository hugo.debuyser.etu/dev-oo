package tp03;

import java.util.Arrays;
import java.time.LocalDate;

public class ToDoList {
    //attributes
    private Task[] chores;

    //constructor
    public ToDoList() {
        this.chores = new Task[5];
    }

    //methods
    public Task[] getChores() {
        return chores;
    }

    public void enlarge() {
        Task []newChores = Arrays.copyOf(this.chores, this.chores.length + 5);
        this.chores = newChores;
    }

    public String toString() {
        String sb = "ToDoList:\n";
        for(int i = 0; i<this.chores.length;i++){
            if (chores[i] != null) {
                sb = sb + chores[i].toString() + "\n";
            }
        }
        return sb;
    }

    public void addTask(Task aTask) {
        for (int i = 0; i < chores.length; i++) {
            if (chores[i] == null) {
                chores[i] = aTask;
                return;
            }
        }
        enlarge();
        addTask(aTask);
    }

    public void removeTask(Task aTask) {
        for (int i = 0; i < chores.length; i++) {
            if (chores[i] == aTask) {
                chores[i] = null;
                return;
            }
        }
    }
    
    public void removeTask(int i) {
        if (i >= 0 && i < chores.length) {
            chores[i] = null;
        }
    }
    
    public boolean isOverwhelmed() {
        for (int i = 0; i < chores.length; i++) {
            if (chores[i] == null) {
                return false;
            }
        }
        return true;
    }

    public int getNbTask() {
        int count = 0;
        for (int i = 0; i < chores.length; i++) {
            if (chores[i] != null) {
                count++;
            }
        }
        return count;
    }
    
    public void onSickLeave(int nbDays) {
        for (int i = 0; i < chores.length; i++) {
            if (chores[i] != null) {
                chores[i].delay(nbDays);
            }
        }
    }

    public Task[] dueTasks() {
        Task[] dueTasksArray = new Task[chores.length];
        int count = 0;
        for (int i = 0; i < chores.length; i++) {
            if (chores[i] != null && chores[i].getDeadline().isEqual(LocalDate.now())) {
                dueTasksArray[count++] = chores[i];
            }
        }
        return Arrays.copyOf(dueTasksArray, count);
    }
    
    public void emergencySort() {
        for (int i = 0; i < chores.length - 1; i++) {
            for (int j = 0; j < chores.length - i - 1; j++) {
                if (chores[j] != null && chores[j + 1] != null &&
                    chores[j].getDeadline().isAfter(chores[j + 1].getDeadline())) {
                    Task temp = chores[j];
                    chores[j] = chores[j + 1];
                    chores[j + 1] = temp;
                }
            }
        }
    }
    
    public void durationSort() {
        for (int i = 0; i < chores.length - 1; i++) {
            for (int j = 0; j < chores.length - i - 1; j++) {
                if (chores[j] != null && chores[j + 1] != null &&
                    chores[j].getDuration() > chores[j + 1].getDuration()) {
                    Task temp = chores[j];
                    chores[j] = chores[j + 1];
                    chores[j + 1] = temp;
                }
            }
        }
    }
    
    public void senioritySort() {
        for (int i = 0; i < chores.length - 1; i++) {
            for (int j = 0; j < chores.length - i - 1; j++) {
                if (chores[j] != null && chores[j + 1] != null &&
                    chores[j].getCreationDate().isAfter(chores[j + 1].getCreationDate())) {
                    Task temp = chores[j];
                    chores[j] = chores[j + 1];
                    chores[j + 1] = temp;
                }
            }
        }
    }    

}

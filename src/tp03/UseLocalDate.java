package tp03;
import util.Keyboard;

import java.time.LocalDate;

public class UseLocalDate {

    public static String dateOfTheDay(){
        return "Today’s date is " + LocalDate.now();
    }

    public static LocalDate inputDate(){
        int annee, mois, jour;

        do{
            annee = Keyboard.readInt("Entrez une année : ");
        }while (annee <= 0);

        do {
            mois = Keyboard.readInt("Entrez un mois : ");
        } while (mois < 1 || mois > 12);

        do {
            jour = Keyboard.readInt("Entrez un jour : " + LocalDate.of(annee, mois, 1).lengthOfMonth() + "): ");
        } while (jour < 1 || jour > LocalDate.of(annee, mois, 1).lengthOfMonth());

        return LocalDate.of(annee, mois, jour);
    }

    public static String diffDate() {
        LocalDate date1 = inputDate();
        LocalDate date2 = inputDate();

        String dates = "Date 1: " + date1 + "\n";
        dates += "Date 2: " + date2 + "\n";

        long diffInDays = date2.toEpochDay() - date1.toEpochDay();
        dates += "Difference in days: " + diffInDays;

        return dates;
    }

}

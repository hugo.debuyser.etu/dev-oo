package tp03;

public class Card {

    //attributs
    private Color color;
    private Rank rank;

    //constructor
    public Card(Color color, Rank rank) {
        this.color = color;
        this.rank = rank;
    }

    //methods
    public Card(String color, String rank) {
        if (color != null) {
            this.color = Color.valueOf(color.toUpperCase());
        } else {
            this.color = null;
        }
        if (rank != null) {
            this.rank = Rank.valueOf(rank.toUpperCase());
        } else {
            this.rank = null;
        }
    }    

    public Color getColor() {
        return color;
    }

    public Rank getRank() {
        return rank;
    }

    public int compareRank(Card otherCard) {
        if (this.rank == null || otherCard.getRank() == null) {
            return 0;
        }
        return this.rank.compareTo(otherCard.getRank());
    }

    public int compareColor(Card otherCard) {
        if (this.color == null || otherCard.getColor() == null) {
            return 0;
        }
        return this.color.compareTo(otherCard.getColor());
    }

    public boolean isBefore(Card otherCard) {
        if (this.rank == null || otherCard.getRank() == null) {
            return false;
        }
        return this.rank.ordinal() < otherCard.getRank().ordinal();
    }

    public boolean equals(Card otherCard) {
        if (this.rank == null || this.color == null || otherCard == null || otherCard == null) {
            return false;
        }
        return this.rank == otherCard.getRank() && this.color == otherCard.getColor();
    }

    public String toString() {
        if (this.rank == null || this.color == null) {
            return "[Carte inconnu]";
        }
        return "[" + rank + " of " + color + "]";
    }
}

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;

public class AgendaTest {
	public Event evt1;
	public Event evt2;
	public Event evt3;
	public Event evt4;
	public Agenda ag = new Agenda();
	public Agenda ag2 = new Agenda();
	
	@BeforeEach
	public void avantTest() {
		ag.clear();
		LocalDate d1 = LocalDate.of(2018, 1, 5);
		LocalDate d2 = LocalDate.of(2018, 2, 10);
		LocalDate d3 = LocalDate.of(2018, 3, 15);
		LocalDate d4 = LocalDate.of(2018, 4, 20);
		evt1 = new Event("evt1", null, d1, d2);
		evt2 = new Event("evt2", null, d2, d3);
		evt3 = new Event("evt3", null, d3, d4); 
		evt4 = new Event("evt4", null, d2, d4);
	}
	
	@Test
	public void testConflicting() {
		assertTrue(ag.conflicting(evt1));
		ag.addEvent(evt1);
		assertTrue(ag.conflicting(evt3));
		ag.addEvent(evt3);
		assertFalse(ag.conflicting(evt4));
	}
	@Test
	public void testRemoveOverlapping() {
		ag.addEvent(evt1);
		ArrayList<Event> tmp = new ArrayList<Event>();
		ag.removeOverlapping(evt1);
		assertEquals(ag.events, tmp);
		ag.addEvent(evt1);ag.addEvent(evt2);tmp.add(evt1); tmp.add(evt2);
		ag.removeOverlapping(evt1);
		ag.addEvent(evt3); tmp.clear();
		ag.removeOverlapping(evt4);
		assertEquals(ag.events,tmp);
		ag.addEvent(evt1);ag.addEvent(evt2);ag.addEvent(evt3); tmp.clear(); tmp.add(evt1);
		ag.removeOverlapping(evt4);
		assertEquals(ag.events,tmp);
	}
}

package tpQU.tp03;

public class UseStudentBUT {
    public static void main(String[] args) {
        Person alice = new Person("Alice", "a");
        StudentBUT1 bruno = new StudentBUT1("Bruno", "b");
        StudentBUT2 clement = new StudentBUT2("Clement", "c");

        System.out.println(alice.toString());
        System.out.println(bruno.toString());
        System.out.println(clement.toString());
    }
}


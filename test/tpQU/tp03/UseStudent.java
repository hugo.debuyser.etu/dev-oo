package tpQU.tp03;

public class UseStudent {
    public static void main(String[] args) {
        Person alice = new Person("Alice", "a");
        Student1 bruno = new Student1("Bruno", "b");
        Student2 clement = new Student2("Clement", "c");

        System.out.println(alice.toString());
        System.out.println(bruno.toString());
        System.out.println(clement.toString());
    }
}

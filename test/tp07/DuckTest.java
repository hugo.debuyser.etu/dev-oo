package tp07;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class DuckTest {
    public int id1, id2, id3;
    public double w1, w2, w3;
    public Duck d1, d2, d3;

    @BeforeEach
    void testInitialization() {
        id1=10; id2=20; id3=30;
        w1=1.0; w2=2.0; w3=3.0;
        d1 = new Duck(id1, w1);
        d2 = new Duck(id2, w2);
        d3 = new Duck(id3, w3);
    }

    @Test
    void testGetIdentity() {
        assertEquals(id1, d1.getIdentity());
        assertEquals(id2, d2.getIdentity());
        assertEquals(id3, d3.getIdentity());
    }

    @Test
    void testSetIdentity() {
        assertEquals(id1, d1.getIdentity());
        d1.setIdentity(id2);
        assertEquals(id2, d1.getIdentity());
    }

    @Test
    void testGetWeight() {
        assertEquals(w1, d1.getWeight());
        assertEquals(w2, d2.getWeight());
        assertEquals(w3, d3.getWeight());
    }

    @Test
    void testSetWeight() {
        assertEquals(w1, d1.getWeight());
        d1.setWeight(w2);
        assertEquals(w2, d1.getWeight());
    }

    @Test
    void testGetPrice() {
        assertEquals(w1*d1.getPriceKg(), d1.getPrice());
        assertEquals(w2*d2.getPriceKg(), d2.getPrice());
        assertEquals(w3*d3.getPriceKg(), d3.getPrice());
    }
}

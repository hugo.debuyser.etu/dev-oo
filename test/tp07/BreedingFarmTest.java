package tp07;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class BreedingFarmTest {
    int id1, id2, id3, id4, id5, id6, size1;
    double w1, w2, w3;
    Duck d1, d2;
    Hen h1, h2;
    Goose g1, g2;
    BreedingFarm bf1, bf2;

    @BeforeEach
    void testInitialization() {
        id1=1; id2=2; id3=10; id4=20; id5=100; id6=200;
        size1=3;
        w1=3.0; w2=6.0; w3=15.0;
        d1 = new Duck(id1, w1);
        d2 = new Duck(id2, w2);
        h1 = new Hen(id3, w1);
        h2 = new Hen(id4, w2);
        g1 = new Goose(id5, w2);
        g2 = new Goose(id6, w3);
        bf1 = new BreedingFarm(size1);
        bf2 = new BreedingFarm();
    }

    @Test
    void testAdd() {
        // bf1 --> élevage taille max = size1
        // bf2 --> élevage taille max par défaut
        assertEquals(0, bf1.getFarmSize());
        assertEquals(0, bf2.getFarmSize());
        bf1.add(d1); bf2.add(d1);
        assertEquals(1, bf1.getFarmSize());
        assertEquals(1, bf2.getFarmSize());
        bf1.add(h1); bf2.add(h1);
        assertEquals(2, bf1.getFarmSize());
        assertEquals(2, bf2.getFarmSize());
        bf1.add(g1); bf2.add(g1);
        assertEquals(3, bf1.getFarmSize());
        assertEquals(3, bf2.getFarmSize());
        bf1.add(d2); bf2.add(d2);
        assertEquals(3, bf1.getFarmSize());
        assertEquals(4, bf2.getFarmSize());
    }

    @Test
    void testGetNbDuck() {
        assertEquals(0, bf1.getNbDuck());
        bf1.add(d1);
        assertEquals(1, bf1.getFarmSize());
        assertEquals(1, bf1.getNbDuck());
        bf1.add(h1);
        assertEquals(2, bf1.getFarmSize());
        assertEquals(1, bf1.getNbDuck());
        bf1.add(d2);
        assertEquals(3, bf1.getFarmSize());
        assertEquals(2, bf1.getNbDuck());
    }

    @Test
    void testSearch() {
        bf1.add(d1); bf1.add(h1); bf1.add(g1);
        assertEquals(3, bf1.getFarmSize());
        assertSame(d1, bf1.search(id1));
        assertNull(bf1.search(id2));
        assertSame(h1, bf1.search(id3));
        assertNull(bf1.search(id4));
        assertSame(g1, bf1.search(id5));
        assertNull(bf1.search(id6));
    }

    @Test
    void testGetDuck() {
        bf1.add(d1); bf1.add(h1); bf1.add(d2);
        assertEquals(3, bf1.getFarmSize());
        assertEquals(2, bf1.getNbDuck());
        assertSame(d1, bf1.getDuck(1));
        assertSame(d2, bf1.getDuck(2));
        assertNull(bf1.getDuck(3));
    }

    @Test
    void testUpdateWeight() {
        bf1.add(d1); bf1.add(h1); bf1.add(g1);
        assertEquals(3, bf1.getFarmSize());
        assertEquals(w1, d1.getWeight());
        bf1.updateWeight(id1, w2);
        assertEquals(w2, d1.getWeight());
    }

    @Test
    void testIsFatEnough() {
        assertFalse(d1.isFatEnough());
        assertTrue(d2.isFatEnough());
        assertFalse(h1.isFatEnough());
        assertTrue(h2.isFatEnough());
        assertFalse(g1.isFatEnough());
        assertTrue(g2.isFatEnough());
    }

    @Test
    void testPotentialProfit() {
        double profit = 0.0;
        assertEquals(0, bf2.getFarmSize());
        assertEquals(profit, bf2.potentialProfit());
        bf2.add(d1); // too thin
        assertEquals(1, bf2.getFarmSize());
        assertEquals(profit, bf2.potentialProfit());
        bf2.add(d2); // can be slaughtered
        profit += d2.getWeight() * d2.getPriceKg();
        assertEquals(2, bf2.getFarmSize());
        assertEquals(profit, bf2.potentialProfit());
        bf2.add(h1); // too thin
        assertEquals(3, bf2.getFarmSize());
        assertEquals(profit, bf2.potentialProfit());
        bf2.add(h2); // can be slaughtered
        profit += h2.getWeight() * h2.getPriceKg();
        assertEquals(4, bf2.getFarmSize());
        assertEquals(profit, bf2.potentialProfit());
        bf2.add(g1); // too thin
        assertEquals(5, bf2.getFarmSize());
        assertEquals(profit, bf2.potentialProfit());
        bf2.add(g2); // can be slaughtered
        profit += g2.getWeight() * g2.getPriceKg();
        assertEquals(6, bf2.getFarmSize());
        assertEquals(profit, bf2.potentialProfit());
    }

    @Test
    void testSlaughtering() {
        List<Poultry> toSlaughter = new ArrayList<Poultry>();
        bf2.add(d1); bf2.add(h1);
        assertEquals(2, bf2.getFarmSize());
        toSlaughter = bf2.slaughtering();
        assertEquals(2, bf2.getFarmSize());
        assertEquals(0, toSlaughter.size());
        bf2.add(d2);
        assertEquals(3, bf2.getFarmSize());
        toSlaughter = bf2.slaughtering();
        assertEquals(2, bf2.getFarmSize());
        assertEquals(1, toSlaughter.size());
        assertEquals(d2, toSlaughter.get(0));
        bf2.add(d2); bf2.add(h2);
        assertEquals(4, bf2.getFarmSize());
        toSlaughter = bf2.slaughtering();
        assertEquals(2, bf2.getFarmSize());
        assertEquals(2, toSlaughter.size());
        assertEquals(d2, toSlaughter.get(0));
        assertEquals(h2, toSlaughter.get(1));
    }

}

package tp07;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class HenTest {
    public int id1, id2, id3;
    public double w1, w2, w3;
    public Hen h1, h2, h3;

    @BeforeEach
    void testInitialization() {
        id1=10; id2=20; id3=30;
        w1=1.0; w2=2.0; w3=3.0;
        h1 = new Hen(id1, w1);
        h2 = new Hen(id2, w2);
        h3 = new Hen(id3, w3);
    }

    @Test
    void testGetIdentity() {
        assertEquals(id1, h1.getIdentity());
        assertEquals(id2, h2.getIdentity());
        assertEquals(id3, h3.getIdentity());
    }

    @Test
    void testSetIdentity() {
        assertEquals(id1, h1.getIdentity());
        h1.setIdentity(id2);
        assertEquals(id2, h1.getIdentity());
    }

    @Test
    void testGetWeight() {
        assertEquals(w1, h1.getWeight());
        assertEquals(w2, h2.getWeight());
        assertEquals(w3, h3.getWeight());
    }

    @Test
    void testSetWeight() {
        assertEquals(w1, h1.getWeight());
        h1.setWeight(w2);
        assertEquals(w2, h1.getWeight());
    }

    @Test
    void testGetPrice() {
        assertEquals(w1*h1.getPriceKg(), h1.getPrice());
        assertEquals(w2*h2.getPriceKg(), h2.getPrice());
        assertEquals(w3*h3.getPriceKg(), h3.getPrice());
    }
}
